//
// ControlScratch.cs
//
// Created by Lazarevich Aleksey on 05.05.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

namespace Geec.Advertisement
{
public class ControlScratch : MonoBehaviour, IPointerExitHandler
{
	public PanelScratchScreen PanelScratch;
	public Image ImageBackground;
	public Image ImageCover;
	
	public Sprite SpriteBackground1;
	public Sprite SpriteBackground2;
	public Sprite SpriteBackground3;
	
	public Sprite SpriteCoverFull;
	public Sprite SpriteCoverVert1;
	public Sprite SpriteCoverVert2;
	public Sprite SpriteCoverHoriz1;
	public Sprite SpriteCoverHoriz2;
	public Sprite SpriteCoverV1H1;
	public Sprite SpriteCoverV1H2;
	public Sprite SpriteCoverV2H1;
	public Sprite SpriteCoverV2H2;
	
	EventTriggerType PointerEnterTrigger;
	EventTriggerType PointerExitTrigger;
	
	enum EventTriggerType
	{
		Right = 0,
		Left,
		Top,
		Bottom,
		None
	}
	
	enum ScratchDirection
	{
		Horizontal,
		Vertical,
		None
	}
	
	
	void Awake()
	{
		ResetTriggers();
	}

	#region IPointerExitHandler implementation
	public void OnPointerExit( PointerEventData eventData )
	{
		//Debug.LogWarning( "OnPointerExit" );
	
		if( ImageCover.enabled )
		{
			UpdateCover( DetectScratchDirection() );
		}
	
		ResetTriggers();
	}
	#endregion
	
	public void TriggerEnter( int type )
	{
		//Debug.Log( "TriggerEnter" );
	
		if( PointerEnterTrigger == EventTriggerType.None )
		{
			if(	type >= (int)EventTriggerType.Right &&
				type < (int)EventTriggerType.None )
			{
				PointerEnterTrigger = (EventTriggerType)type;
			}
		}
	}
	
	public void TriggerExit( int type )
	{
		//Debug.Log( "TriggerExit" );
	
		if(	type >= (int)EventTriggerType.Right &&
			type < (int)EventTriggerType.None )
		{
			PointerExitTrigger = (EventTriggerType)type;
		}
	}
	
	void OnEnable()
	{
		ImageCover.sprite = SpriteCoverFull;
		ImageCover.enabled = true;
		ResetTriggers();
	}
	
	void ResetTriggers()
	{
		PointerEnterTrigger = EventTriggerType.None;
		PointerExitTrigger = EventTriggerType.None;
	}
	
	void UpdateCover( ScratchDirection scratchDirection )
	{	
		if( scratchDirection == ScratchDirection.None )
		{
			return;
		}
		
		if( ImageCover.sprite == SpriteCoverFull )
		{
			switch( scratchDirection )
			{
				case ScratchDirection.Horizontal:
				{
					ImageCover.sprite = SpriteCoverHoriz1;
					PanelScratch.OnScratch( this );
					break;
				}
				
				case ScratchDirection.Vertical:
				{
					ImageCover.sprite = SpriteCoverVert1;
					PanelScratch.OnScratch( this );
					break;
				}
			}
		}
		else if( ImageCover.sprite == SpriteCoverHoriz1 )
		{
			switch( scratchDirection )
			{
				case ScratchDirection.Horizontal:
				{
					ImageCover.sprite = SpriteCoverHoriz2;
					break;
				}
				
				case ScratchDirection.Vertical:
				{
					ImageCover.sprite = SpriteCoverV1H1;
					break;
				}
			}
		}
		else if( ImageCover.sprite == SpriteCoverHoriz2 )
		{
			switch( scratchDirection )
			{
				case ScratchDirection.Horizontal:
				{
					ImageCover.enabled = false;
					break;
				}
				
				case ScratchDirection.Vertical:
				{
					ImageCover.sprite = SpriteCoverV1H2;
					break;
				}
			}
		}
		else if( ImageCover.sprite == SpriteCoverVert1 )
		{
			switch( scratchDirection )
			{
				case ScratchDirection.Horizontal:
				{
					ImageCover.sprite = SpriteCoverV1H1;
					break;
				}
				
				case ScratchDirection.Vertical:
				{
					ImageCover.sprite = SpriteCoverVert2;
					break;
				}
			}
		}
		else if( ImageCover.sprite == SpriteCoverVert2 )
		{
			switch( scratchDirection )
			{
				case ScratchDirection.Horizontal:
				{
					ImageCover.sprite = SpriteCoverV2H1;
					break;
				}
				
				case ScratchDirection.Vertical:
				{
					ImageCover.enabled = false;
					break;
				}
			}
		}
		else if( ImageCover.sprite == SpriteCoverV1H1 )
		{
			switch( scratchDirection )
			{
				case ScratchDirection.Horizontal:
				{
					ImageCover.sprite = SpriteCoverV1H2;
					break;
				}
				
				case ScratchDirection.Vertical:
				{
					ImageCover.sprite = SpriteCoverV2H1;
					break;
				}
			}
		}
		else if( ImageCover.sprite == SpriteCoverV1H2 )
		{
			switch( scratchDirection )
			{
				case ScratchDirection.Horizontal:
				{
					ImageCover.enabled = false;
					break;
				}
				
				case ScratchDirection.Vertical:
				{
					ImageCover.sprite = SpriteCoverV2H2;
					break;
				}
			}
		}
		else if( ImageCover.sprite == SpriteCoverV2H1 )
		{
			switch( scratchDirection )
			{
				case ScratchDirection.Horizontal:
				{
					ImageCover.sprite = SpriteCoverV2H2;
					break;
				}
				
				case ScratchDirection.Vertical:
				{
					ImageCover.enabled = false;
					break;
				}
			}
		}
		else if( ImageCover.sprite == SpriteCoverV2H2 )
		{
			if( scratchDirection != ScratchDirection.None )
			{
				ImageCover.enabled = false;
			}
		}
	}
	
	ScratchDirection DetectScratchDirection()
	{
		if( PointerEnterTrigger == EventTriggerType.None ||
			PointerExitTrigger == EventTriggerType.None )
		{
			return ScratchDirection.None;
		}
		
		if( PointerEnterTrigger == PointerExitTrigger )
		{
			if( PointerEnterTrigger == EventTriggerType.Left ||
				PointerEnterTrigger == EventTriggerType.Right )
			{
				return ScratchDirection.Horizontal;
			}
			else
			{
				return ScratchDirection.Vertical;
			}
		}
		else
		{
			switch( PointerEnterTrigger )
			{
				case EventTriggerType.Top:
				case EventTriggerType.Bottom:
				{
					return ScratchDirection.Vertical;
				}
				
				case EventTriggerType.Left:
				case EventTriggerType.Right:
				{
					if( PointerExitTrigger == EventTriggerType.Left ||
						PointerExitTrigger == EventTriggerType.Right )
					{
						return ScratchDirection.Horizontal;
					}
					else
					{
						return ScratchDirection.Vertical;
					}
				}
			}
		}
		
		return ScratchDirection.None;
	}
}
}