﻿//
// PopupAppearence.cs
//
// Created by Lazarevich Aleksey on 3.05.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Geec.Advertisement
{
[RequireComponent(typeof(RectTransform))]
public class PopupAppearence : MonoBehaviour 
{
	public RectTransform rectTransform;
	[Range (0, 10)] public int AppearenceTime = 1;
	[Range (0, 10)] public int ShowTime = 5;
	
	Vector2 source;
	
	
	void OnEnable()
	{
		if( rectTransform.anchorMin.x == rectTransform.anchorMax.x &&
			rectTransform.anchorMax.x == 1 )
		{
			rectTransform.anchoredPosition = new Vector2( rectTransform.rect.width, rectTransform.anchoredPosition.y );
		}
			else if( rectTransform.anchorMin.x == rectTransform.anchorMax.x &&
					 rectTransform.anchorMax.x == 0 )
		{
			rectTransform.anchoredPosition = new Vector2( -rectTransform.rect.width, rectTransform.anchoredPosition.y );
		}
		
		source = rectTransform.anchoredPosition;
		StartCoroutine( Appear( new Vector2( 0, rectTransform.anchoredPosition.y ) ) );
	}
	
	IEnumerator Appear( Vector2 target, bool hide = false )
	{
		if( AppearenceTime <= 0f )
		{
			AppearenceTime = 1;
		}
		
		float divisions = rectTransform.rect.width/AppearenceTime;
	
		while( rectTransform.anchoredPosition.x != target.x )
		{
			rectTransform.anchoredPosition = Vector2.MoveTowards( rectTransform.anchoredPosition, target, divisions*Time.deltaTime );
			yield return null;
		}
		
		if( hide )
		{
			gameObject.SetActive( false );
		}
		else
		{
			StartCoroutine( Showing() );
		}
	}
	
	IEnumerator Showing()
	{
		float timer = 0;
		
		while( timer < ShowTime )
		{
			timer += Time.deltaTime;
			yield return null;
		}
		
		StartCoroutine( Appear( source, true ) );
	}
}
}