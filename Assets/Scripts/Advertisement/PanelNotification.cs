//
// PanelNotification.cs
//
// Created by Lazarevich Aleksey on 3.05.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Geec.Advertisement
{
public class PanelNotification : MonoBehaviour 
{
	public AdvertisementViewConroller Controller;
	public GameObject QuizVictoryPopup;


	public void ShowQuizVictoryPopup()
	{
		if( QuizVictoryPopup != null )
		{
			QuizVictoryPopup.SetActive( true );
		}
	}
	
	public void OnQuizVictoryPopupClick()
	{
		QuizVictoryPopup.SetActive( false );
		Controller.OnQuizVictoryPopupClick();
	}
}
}