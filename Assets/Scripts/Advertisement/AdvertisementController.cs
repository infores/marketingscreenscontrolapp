//
// AdvertisementController.cs
//
// Created by Lazarevich Aleksey on 28.04.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Geec.Advertisement
{
public interface IAdvertisementControllerListener
{
	void AdvertisementClosed();
}

public interface IAdvertisementController
{
	void SetListener( IAdvertisementControllerListener listener );
	void ShowAdvertisement();
	void ShowReminder();
	void ShowNotification();
}

[ExecuteInEditMode]
public class AdvertisementController : MonoBehaviour, IAdvertisementController
{
	public AdvertisementConfigurator Configurator;
	public AdvertisementViewConroller View;
	
	static IAdvertisementController AdvertisementControllerInstance;
	protected IAdvertisementControllerListener listener;
	protected AdvertisementData data;
	List<byte> promotionsToRemind;
	bool quizVictory = false;
	List<byte> wonQuizes;

		
	void Awake()
	{
		DontDestroyOnLoad( gameObject );
		
		LoadAllData();
	}
	
	public static IAdvertisementController Instance() 
	{
		if( AdvertisementControllerInstance == null )
		{
			AdvertisementController adsCntrl = FindObjectOfType<AdvertisementController>();
			if( adsCntrl == null )
			{
				Debug.LogError( "There needs to be one active AdvertisementController script on a GameObject in your scene." );
			}
			else
			{
				AdvertisementControllerInstance = adsCntrl as IAdvertisementController;
			}
		}
		
		return AdvertisementControllerInstance;
	}
	
	#region IAdvertisementController implementation
	public void SetListener( IAdvertisementControllerListener listener )
	{
		this.listener = listener;
	}
		
	public void ShowAdvertisement()
	{
		StartCoroutine( ShowAdvertisementCoroutine() );
	}
	
	public void ShowReminder()
	{
		StartCoroutine( ShowReminderCoroutine() );
	}
	
	public void ShowNotification()
	{
		if( quizVictory )
		{
			View.ShowQuizVictoryPopup();
		}
	}
	#endregion
	
	IEnumerator ShowReminderCoroutine()
	{
		yield return null;
	
		if( promotionsToRemind.Count > 0 &&
		   data != null &&
		   data.Promotions != null &&
		   promotionsToRemind[0] < data.Promotions.Count )
		{			
			if( !View.ShowPromotion( data.Promotions[promotionsToRemind[0]], true ) )
			{
				if( listener != null )
				{
					listener.AdvertisementClosed();
				}
			}
			
			promotionsToRemind.RemoveAt(0);
			
			Configurator.SavePromotionsToRemind( promotionsToRemind );
		}
		else
		{
			if( listener != null )
			{
				listener.AdvertisementClosed();
			}
		}
	}
	
	IEnumerator ShowAdvertisementCoroutine()
	{
		yield return null;
	
		bool showPromotion = true;
		int max = Configurator.PromotionChance + Configurator.QuizChance + Configurator.ScratchChance;
		int value = Random.Range( 0, max );
		
		if( value > (Configurator.PromotionChance + Configurator.QuizChance) )
		{
			if( data.ScratchData != null )
			{
				showPromotion = !View.ShowScratch( data.ScratchData );
			}
		}
		else if( value > Configurator.PromotionChance &&
				!quizVictory )
		{
			QuizData quizData = GetQuizToShow();
			
			if( quizData != null )
			{				
				showPromotion = !View.ShowQuiz( quizData );
			}
		}
		
		if( showPromotion )
		{
			if( data != null &&
				data.Promotions != null &&
				data.Promotions.Count > 0 )
			{
				if( !View.ShowPromotion( data.Promotions[ Random.Range( 0, data.Promotions.Count ) ] ) )
				{
					if( listener != null )
					{
						listener.AdvertisementClosed();
					}
				}
			}
			else
			{
				if( listener != null )
				{
					listener.AdvertisementClosed();
				}
			}
		}
	}
	
	public void AdvertisementClosed()
	{
		if( listener != null )
		{
			listener.AdvertisementClosed();
		}
	}
	
	virtual public void PromotionClosed( PromotionData promotion = null, bool remind = false )
	{
		if( remind &&
			promotion != null &&
			data != null &&
			data.Promotions != null )
		{
			int index = data.Promotions.IndexOf( promotion );
			if( index != -1 )
			{
				promotionsToRemind.Add( (byte)index );
			}
		}
	
		if( listener != null )
		{
			listener.AdvertisementClosed();
		}
	}
	
	virtual public void QuizClosed( QuizData quiz, int answerIndex )
	{
		int index = data.Quizes.IndexOf( quiz );
		if( index != -1 )
		{
			if( data.Quizes[index].Question.CorrectAnswer == answerIndex )
			{				
				quizVictory = true;
				Configurator.SaveQuizVictory( quizVictory );
				
				wonQuizes.Add( (byte)index );
				Configurator.SaveWonQuizes( wonQuizes );
			}
		}
	
		if( listener != null )
		{
			listener.AdvertisementClosed();
		}
	}
	
	virtual public void ScratchClosed( ScratchData scratch, bool victory )
	{
		if( listener != null )
		{
			listener.AdvertisementClosed();
		}
	}
	
	void LoadAllData()
	{
		data = Configurator.LoadConfig();
		
		promotionsToRemind = Configurator.LoadPromotionsToRemind();
		quizVictory = Configurator.LoadQuizVictory();
		wonQuizes = Configurator.LoadWonQuizes();
	}
	
	QuizData GetQuizToShow()
	{
		if( data == null ||
			data.Quizes == null ||
			data.Quizes.Count == 0 )
		{
			return null;
		}
		
		for( int i=0; i<data.Quizes.Count; i++ )
		{
			if( !wonQuizes.Contains( (byte)i ) )
			{
				return data.Quizes[i];
			}
		}
		
		return null;
	}
	
	virtual public void QuizVicroryPopupShown()
	{
		quizVictory = false;
		Configurator.SaveQuizVictory( quizVictory );
	}
	
	virtual public void QuizVictoryClosed()
	{}
}
}