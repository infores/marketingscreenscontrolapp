﻿//
// PanelQuizVictory.cs
//
// Created by Lazarevich Aleksey on 3.05.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Geec.Advertisement
{
public class PanelQuizVictory : AdvertisementScreen 
{	
	public void OnVisitClick()
	{
		Application.OpenURL( "http://www.google.ru" );
		gameObject.SetActive( false );
		
		Controller.QuizVictoryClosed();
	}
}
}