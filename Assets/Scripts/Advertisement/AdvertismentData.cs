//
// Advertisement.cs
//
// Created by Lazarevich Aleksey on 28.04.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Geec.Advertisement
{
public enum AdvertisementType
{
	Promotion,
	Quiz,
	Scratch
}

public enum ImageType
{
	Background,
	Goods,
	Logo,
	Sale
}

[Serializable]
[XmlRoot("AdvertisementData")]
public class AdvertisementData
{
	public int PromotionChance;
	public int QuizChance;
	public int ScratchChance;
	
	[XmlArray("Promotions")]
	[XmlArrayItem("PromotionData")]
	public List<PromotionData> Promotions = new List<PromotionData>();
	
	[XmlArray("Quizes")]
	[XmlArrayItem("QuizData")]
	public List<QuizData> Quizes = new List<QuizData>();
	
	public ScratchData ScratchData = new ScratchData();
}


[Serializable]
public class PromotionData
{
	public PromotionLayout GoodsLayout;
	public PromotionLayout SaleLayout;
	public String GoodsImageName = "";
	public String BackgroundImageName = ""; 
	public String SaleImageName = "";
	[Range (0, 99)] public int SaleAmount;
	[Range (0, 359)] public int SaleRotation;
	public PromotionLayout TextLayout;
	public String Description = "";
	public float Price;
	public CurrencyType Currency;
	[Range (0, 10)] public int SkipTime;
	public String HttpLink = "";
	
	
	public enum PromotionLayout
	{
		Middle,
		MiddleLeft,
		MiddleRight,
		TopMiddle,
		TopLeft,
		TopRight,
		BottomMiddle,
		BottomLeft,
		BottomRight
	}
	
	public enum CurrencyType
	{
		Euro,
		Dollar,
		Pound
	}
}


[Serializable]
public class QuizData
{
	public String LogoImageName = "";
	public String BackgroundImageName = "";
	public QuestionData Question;
	
	[Serializable]
	public class QuestionData
	{
		public String QuestionText = "";
		public Color QuestionTextColor;
		public String Answer1 = "";
		public Color Answer1Color;
		public String Answer2 = "";
		public Color Answer2Color;
		public String Answer3 = "";
		public Color Answer3Color;
		[Range (0, 2)] public int CorrectAnswer = 0;
	}
}


[Serializable]
public class ScratchData
{
	public String LogoImageName = "";
	public String BackgroundImageName = "";
	public String WonMessage = "";
	public Color WonMessageColor;
	public String LostMessage = "";
	public Color LostMessageColor;
	public String WinUrl = "";
}
}