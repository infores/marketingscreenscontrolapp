//
// AdvertisementConfigurator.cs
//
// Created by Lazarevich Aleksey on 28.04.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

namespace Geec.Advertisement
{
public class AdvertisementConfigurator : MonoBehaviour 
{
	[Range(1, 100)] public int PromotionChance = 60;
	[Range(1, 100)] public int QuizChance = 30;
	[Range(0, 100)] public int ScratchChance;
	public List<PromotionData> Promotions;
	public List<QuizData> Quizes;
	public ScratchData ScratchData;
	
#region Save/Load constants
	const string configFilePathSave = "Resources/Advertisement/config.xml";
	const string configFilePathLoad = "Advertisement/config";
	
	const string saveNamePromotionsToRemind = "ads.ptr";
	const string saveNameQuizVictory = "ads.qv";
	const string saveNameWonQuizes = "ads.wq";
	const byte maxPromotionsToRemind = byte.MaxValue;
#endregion
	
	public void OnInspectorSave()
	{
		AdvertisementData data = new AdvertisementData();
		data.PromotionChance = PromotionChance;
		data.QuizChance = QuizChance;
		data.ScratchChance = ScratchChance;
		
		data.Promotions = Promotions;
		CheckPromotions( data.Promotions );
		
		data.Quizes = Quizes;
		data.ScratchData = ScratchData;
		
		Save( data, Path.Combine( Application.dataPath, configFilePathSave ) );
	}
	
	public void OnInspectorLoad()
	{
		AdvertisementData data = Load<AdvertisementData>( configFilePathLoad );
		
		if( data != null )
		{
			PromotionChance = data.PromotionChance;
			QuizChance = data.QuizChance;
			ScratchChance = data.ScratchChance;
			
			Promotions = data.Promotions;
			Quizes = data.Quizes;
			ScratchData = data.ScratchData;
		}
	}
	
	public void Save( AdvertisementData data, string filePath )
	{	
		try
		{		
			var serializer = new XmlSerializer( typeof(AdvertisementData) );
			
			using( StreamWriter sw = new StreamWriter( filePath, false, System.Text.Encoding.UTF8 ) )
			{
				serializer.Serialize( sw, data );
			}
			
			Debug.LogWarning( "Saved." );
		}
		catch( Exception ex )
		{
			Debug.LogError( "AdvertisementConfigurator::Save(): " + ex.Message );
		}
	}
	
	public AdvertisementData LoadConfig()
	{
		return Load<AdvertisementData>( configFilePathLoad );
	}
	
	public T Load<T>( string filePath )
	{
		T data = default(T);
		
		try
		{
			TextAsset xml = Resources.Load<TextAsset>( filePath );
			
			if( xml != null )
			{
				var serializer = new XmlSerializer( typeof(AdvertisementData) );
				using( MemoryStream stream = new MemoryStream( xml.bytes ) )
				{
					data = (T)serializer.Deserialize(stream);
				}
			}
			else
			{
				Debug.LogWarning( "AdvertisementConfigurator::Load(): Can't load file." );
			}
		}
		catch( Exception ex ) 
		{
			Debug.LogError( "AdvertisementConfigurator::Load(): " + ex.Message );
		}
		
		return data;
	}
	
	void CheckPromotions( List<PromotionData> Promotions )
	{
		if( Promotions == null )
		{
			return;
		}
		
		for( int i=0; i<Promotions.Count; i++ )
		{
			if( !Uri.IsWellFormedUriString( Promotions[i].HttpLink, UriKind.Absolute ) )
			{
				
			}
		}
		
//		foreach( PromotionData data in Promotions )
//		{
//			bool res = Uri.IsWellFormedUriString( data.HttpLink, UriKind.Absolute );
//			
//			Uri result; 
//			Uri.TryCreate( data.HttpLink, UriKind.Absolute, out result );
//
//			res = Uri.IsWellFormedUriString( data.HttpLink, UriKind.Absolute );
//			
//			res = res;
//		}
	}
	
	string CorrectUrl( string url )
	{
//		const string pref1 = "http://";
//		const string pref2 = "www.";
//	
//		if( url == null )
//		{
//			return null;
//		}
//		
//		string result;
		
		//if(  )
		
		return null;
	}
	
	public List<byte> LoadPromotionsToRemind()
	{
		List<byte> promotionsToRemind = new List<byte>();
	
		try
		{
			string loadString = PlayerPrefs.GetString( saveNamePromotionsToRemind );
			byte[] arr = System.Convert.FromBase64String( loadString );
			promotionsToRemind = new List<byte>( arr );
		}
		catch( Exception ex )
		{
			Debug.LogError( "AdvertisementConfigurator::LoadPromotionsToRemind(): " + ex.Message );
			string empty = "";
			PlayerPrefs.SetString( saveNamePromotionsToRemind, empty );
		}
		
		return promotionsToRemind;
	}
	
	public void SavePromotionsToRemind( List<byte> promotionsToRemind )
	{
		try
		{
			if( promotionsToRemind.Count > maxPromotionsToRemind )
			{
				promotionsToRemind.RemoveRange( maxPromotionsToRemind, promotionsToRemind.Count-maxPromotionsToRemind );
			}
		
			byte[] arr = promotionsToRemind.ToArray();
			string saveString = System.Convert.ToBase64String( arr );
			PlayerPrefs.SetString( saveNamePromotionsToRemind, saveString );
		}
		catch( Exception ex )
		{
			Debug.LogError( "AdvertisementConfigurator::SavePromotionsToRemind(): " + ex.Message );
		}
	}
	
	public void SaveQuizVictory( bool quizVictory )
	{
		PlayerPrefs.SetInt( saveNameQuizVictory, quizVictory? 1: 0 );
	}
	
	public bool LoadQuizVictory()
	{
		return PlayerPrefs.GetInt( saveNameQuizVictory ) == 1? true: false;
	}
	
	public List<byte> LoadWonQuizes()
	{
		List<byte> wonQuizes = new List<byte>();
		
		try
		{
			string loadString = PlayerPrefs.GetString( saveNameWonQuizes );
			byte[] arr = System.Convert.FromBase64String( loadString );
			wonQuizes = new List<byte>( arr );
		}
		catch( Exception ex )
		{
			Debug.LogError( "AdvertisementConfigurator::LoadWonQuizes(): " + ex.Message );
			string empty = "";
			PlayerPrefs.SetString( saveNameWonQuizes, empty );
		}
		
		return wonQuizes;
	}
	
	public void SaveWonQuizes( List<byte> wonQuizes )
	{
		try
		{		
			byte[] arr = wonQuizes.ToArray();
			string saveString = System.Convert.ToBase64String( arr );
			PlayerPrefs.SetString( saveNameWonQuizes, saveString );
		}
		catch( Exception ex )
		{
			Debug.LogError( "AdvertisementConfigurator::SaveWonQuizes(): " + ex.Message );
		}
	}
	
	public void ResetRegistry()
	{
		string empty = "";
		PlayerPrefs.SetString( saveNamePromotionsToRemind, empty );
		PlayerPrefs.SetString( saveNameWonQuizes, empty );
		PlayerPrefs.SetInt( saveNameQuizVictory, 0 );
	}
}
}