//
// PanelQuiz.cs
//
// Created by Lazarevich Aleksey on 28.04.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

namespace Geec.Advertisement
{
public class PanelQuizScreen : AdvertisementScreen 
{
	public Image ImageBackground;
	public Image ImageLogo;
	public Text TextQuestion;
	public Text TextAnswer1;
	public Text TextAnswer2;
	public Text TextAnswer3;
	public Toggle ToogleAnswer1;
	public Toggle ToogleAnswer2;
	public Toggle ToogleAnswer3;
	public Button ButtonGoon;

	QuizData data;
	int answerIndex;
	

	public bool Show( QuizData data )
	{
		if( data == null )
		{
			Debug.LogWarning( "PanelQuiz::Show(): Wrong data." );
			return false;
		}
		
		this.data = data;
		
		if( !LoadImages() )
		{
			return false;
		}
		
		if( !SetupMask() )
		{
			return false;
		}
		
		if( !SetupText() )
		{
			return false;
		}
		
		if( !SetupButtons() )
		{
			return false;
		}
		
		gameObject.SetActive( true );
		StartCoroutine( ShowPanel() );
		
		return true;
	}
	
	bool LoadImages()
	{
		bool result = true;
		
		try
		{
			Sprite sprite = Controller.LoadSprite( data.BackgroundImageName );
			if( sprite == null )
			{
				return false;
			}
			else
			{
				ImageBackground.sprite = sprite;
			}
			
			sprite = Controller.LoadSprite( data.LogoImageName, ImageType.Logo );
			if( sprite == null )
			{
				return false;
			}
			else
			{
				ImageLogo.sprite = sprite;
			}
		}
		catch( Exception ex )
		{
			Debug.LogError( "PanelQuiz::LoadImages(): " + ex.Message );
			result = false;
		}
		
		return result;
	}
	
	bool SetupText()
	{
		bool result = true;
		
		try
		{
			TextQuestion.text = data.Question.QuestionText;
			TextQuestion.color = data.Question.QuestionTextColor;
			
			TextAnswer1.text = data.Question.Answer1;
			TextAnswer2.text = data.Question.Answer2;
			TextAnswer3.text = data.Question.Answer3;
			
			TextAnswer1.color = data.Question.Answer1Color;
			TextAnswer2.color = data.Question.Answer2Color;
			TextAnswer3.color = data.Question.Answer3Color;
			
		}
		catch( Exception ex )
		{
			Debug.LogError( "PanelQuiz::SetupText(): " + ex.Message );
			result = false;
		}
		
		return result;
	}
	
	bool SetupButtons()
	{
		bool result = true;
		
		try
		{			
			ToogleAnswer1.interactable = false;
			ToogleAnswer2.interactable = false;
			ToogleAnswer3.interactable = false;
			
			ToogleAnswer1.isOn = false;
			ToogleAnswer2.isOn = false;
			ToogleAnswer3.isOn = false;
						
			ButtonGoon.gameObject.SetActive( false );
			
			answerIndex = -1;
		}
		catch( Exception ex )
		{
			Debug.LogError( "PanelQuiz::SetupButtons(): " + ex.Message );
			result = false;
		}
			
		return result;
	}
	
	IEnumerator ShowPanel()
	{				
		yield return StartCoroutine( ShowScreen() );
		
		ToogleAnswer1.interactable = true;
		ToogleAnswer2.interactable = true;
		ToogleAnswer3.interactable = true;
	}
	
	public void OnAnswerClick( int answerIndex )
	{
		if( ButtonGoon != null )
		{
			ButtonGoon.gameObject.SetActive( true );
		}
		
		this.answerIndex = answerIndex;
	}
	
	public void OnGoonClick()
	{
		StartCoroutine( Exit(answerIndex) );
	}
	
	IEnumerator Exit( int answerIndex )
	{
		yield return StartCoroutine( HideScreen() );
		
		gameObject.SetActive( false );
		
		Controller.QuizClosed( data, answerIndex );
	}
}
}