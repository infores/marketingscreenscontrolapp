//
// PanelPromotion.cs
//
// Created by Lazarevich Aleksey on 28.04.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

namespace Geec.Advertisement
{
[ExecuteInEditMode]
public class PanelPromotionScreen : AdvertisementScreen 
{
	public Image ImageBackground;
	public RectTransform PanelLayoutImages;
	public Image ImageGoods;
	public Image ImageSale;
	public Text TextSale;
	public RectTransform PanelLayoutTexts;
	public Text TextDescription;
	public Text TextPrice;
	public Text TextRemind;
	public Button ButtonRemind;
	public Button ButtonSkip;
	public Text TextSkip;
	public Button ButtonVisit;

	PromotionData data;
	string stringSkipAd = "Skip ad";
	string stringSaleTemplate = "-{0}%";
	float skipTime = 0;
	
	
	public bool Show( PromotionData data, bool reminder = false )
	{
		if( data == null ||
			data.GoodsImageName == null ||
			data.BackgroundImageName == null )
		{
			Debug.LogWarning( "PanelPromotion::Show(): Wrong data." );
			return false;
		}
		
		this.data = data;
		
		
		if( !LoadImages() )
		{
			return false;
		}
		
		if( !SetupTexts() )
		{
			return false;
		}
		
		if( !SetupLayout( reminder ) )
		{
			return false;
		}
		
		if( !SetupMask() )
		{
			return false;
		}
		
		gameObject.SetActive( true );
		StartCoroutine( ShowPanel() );
		
		if( !reminder )
		{
			skipTime = data.SkipTime;
			StartCoroutine( "SkipTimer" );
		}
		
		return true;
	}
	
	IEnumerator ShowPanel()
	{				
		yield return StartCoroutine( ShowScreen() );
		
		ButtonRemind.enabled = true;
		ButtonVisit.enabled = true;
	}
	
	bool LoadImages()
	{
		bool result = true;
		
		try
		{
			Sprite sprite = Controller.LoadSprite( data.BackgroundImageName );
			if( sprite == null )
			{
				return false;
			}
			else
			{
				ImageBackground.sprite = sprite;
			}
			
			sprite = Controller.LoadSprite( data.GoodsImageName, ImageType.Goods );
			if( sprite == null )
			{
				return false;
			}
			else
			{
				ImageGoods.sprite = sprite;
			}
			
			if( data.SaleAmount > 0 )
			{
				sprite = Controller.LoadSprite( data.SaleImageName, ImageType.Sale );
				if( sprite == null )
				{
					return false;
				}
				else
				{
					ImageSale.sprite = sprite;
					ImageSale.enabled = true;
					TextSale.enabled = true;
				}
			}
			else
			{
				ImageSale.enabled = false;
				TextSale.enabled = false;
			}
		}
		catch( Exception ex )
		{
			Debug.LogError( "PanelPromotion::LoadImages(): " + ex.Message );
			result = false;
		}
		
		return result;
	}
	
	bool SetupLayout( bool reminder )
	{
		bool result = true;
	
		try
		{
			SetPromotionLayout( data.GoodsLayout, PanelLayoutImages );		
			
			if( ImageSale.sprite != null )
			{
				// нужно выставить цент вращения в центр картинки перед вращением
				SetPromotionLayout( PromotionData.PromotionLayout.Middle, ImageSale.rectTransform );
				ImageSale.rectTransform.localRotation = Quaternion.Euler( new Vector3(0, 0, data.SaleRotation) );
				
				SetPromotionLayout( data.SaleLayout, ImageSale.rectTransform );
			}
			
			SetPromotionLayout( data.TextLayout, PanelLayoutTexts );
			
			if( !reminder )
			{
				ButtonRemind.gameObject.SetActive( true );
				ButtonSkip.gameObject.SetActive( true );
				ButtonVisit.gameObject.SetActive( false );
					
				if( data.SkipTime < 0 )
				{
					data.SkipTime = 0;
				}
				UpdateButtonSkip( data.SkipTime );
			}
			else
			{
				ButtonRemind.gameObject.SetActive( false );
				ButtonSkip.gameObject.SetActive( false );
				ButtonVisit.gameObject.SetActive( true );
			}
			
			ButtonRemind.enabled = false;
			ButtonVisit.enabled = false;
			
			
			if( data.SaleAmount > 0 )
			{
				TextSale.text = string.Format( stringSaleTemplate, data.SaleAmount );
			}
		}
		catch( Exception ex )
		{
			Debug.LogError( "PanelPromotion::SetLayout(): " + ex.Message );
			result = false;
		}
		
		return result;
	}
	
	void SetPromotionLayout( PromotionData.PromotionLayout layout, RectTransform rectTransform )
	{
		if( rectTransform == null )
		{
			return;
		}
		
		float halfWidth = rectTransform.rect.width/2f;
		float halfHeight = rectTransform.rect.height/2f;
		
		switch( layout )
		{
			case PromotionData.PromotionLayout.TopLeft:
			{
				rectTransform.anchoredPosition = new Vector2( halfWidth, -halfHeight );
				rectTransform.anchorMin = new Vector2( 0, 1 );
				rectTransform.anchorMax = new Vector2( 0, 1 );
				break;
			}
			
			case PromotionData.PromotionLayout.TopMiddle:
			{
				rectTransform.anchoredPosition = new Vector2( 0, -halfHeight );
				rectTransform.anchorMin = new Vector2( 0.5f, 1 );
				rectTransform.anchorMax = new Vector2( 0.5f, 1 );
				break;
			}
			
			case PromotionData.PromotionLayout.TopRight:
			{
				rectTransform.anchoredPosition = new Vector2( -halfWidth, -halfHeight );
				rectTransform.anchorMin = new Vector2( 1, 1 );
				rectTransform.anchorMax = new Vector2( 1, 1 );
				break;
			}
			
			case PromotionData.PromotionLayout.MiddleLeft:
			{
				rectTransform.anchoredPosition = new Vector2( halfWidth, 0 );
				rectTransform.anchorMin = new Vector2( 0, 0.5f );
				rectTransform.anchorMax = new Vector2( 0, 0.5f );
				break;
			}
			
			case PromotionData.PromotionLayout.Middle:
			{
				rectTransform.anchoredPosition = new Vector2( 0, 0 );
				rectTransform.anchorMin = new Vector2( 0.5f, 0.5f );
				rectTransform.anchorMax = new Vector2( 0.5f, 0.5f );
				break;
			}
			
			case PromotionData.PromotionLayout.MiddleRight:
			{
				rectTransform.anchoredPosition = new Vector2( -halfWidth, 0 );
				rectTransform.anchorMin = new Vector2( 1, 0.5f );
				rectTransform.anchorMax = new Vector2( 1, 0.5f );
				break;
			}
			
			case PromotionData.PromotionLayout.BottomLeft:
			{
				rectTransform.anchoredPosition = new Vector2( halfWidth, halfHeight );
				rectTransform.anchorMin = new Vector2( 0, 0 );
				rectTransform.anchorMax = new Vector2( 0, 0 );
				break;
			}
			
			case PromotionData.PromotionLayout.BottomMiddle:
			{
				rectTransform.anchoredPosition = new Vector2( 0, halfHeight );
				rectTransform.anchorMin = new Vector2( 0.5f, 0 );
				rectTransform.anchorMax = new Vector2( 0.5f, 0 );
				break;
			}
			
			case PromotionData.PromotionLayout.BottomRight:
			{
				rectTransform.anchoredPosition = new Vector2( -halfWidth, halfHeight );
				rectTransform.anchorMin = new Vector2( 1, 0 );
				rectTransform.anchorMax = new Vector2( 1, 0 );
				break;
			}
		}
	}
	
	void UpdateButtonSkip( int time )
	{
		if( time > 0 )
		{
			ButtonSkip.enabled = false;
			TextSkip.text = time.ToString();
		}
		else
		{
			ButtonSkip.enabled = true;
			TextSkip.text = stringSkipAd;
		}
	}
	
	IEnumerator SkipTimer()
	{	
		while( skipTime > 0 )
		{
			skipTime -= Time.deltaTime;
			UpdateButtonSkip( (int)skipTime );
			yield return null;
		}
		
		UpdateButtonSkip( (int)skipTime );
	}
	
	public void OnRemindClick()
	{
		StartCoroutine( Exit( true ) );
	}
	
	public void OnSkipClick()
	{
		StartCoroutine( Exit() );
	}
	
	public IEnumerator Exit( bool remind = false )
	{
		yield return StartCoroutine( HideScreen() );
		
		gameObject.SetActive( false );
		
		Controller.PromotionClosed( data, remind );
	}
	
	public void OnVisitClick()
	{
		Application.OpenURL( data.HttpLink );
		
		StartCoroutine( Exit() );
	}
	
	bool SetupTexts()
	{
		bool result = true;
	
		try
		{
			TextDescription.text = data.Description;
			TextPrice.text = string.Format( "Price: {0}{1}", data.Price, GetCurrencySimbol( data.Currency ) );
		}
		catch( Exception ex )
		{
			Debug.LogError( "PanelPromotion::SetupTexts(): " + ex.Message );
			result = false;
		}
		
		return result;	
	}
	
	string GetCurrencySimbol( PromotionData.CurrencyType currencyType )
	{
		switch( currencyType )
		{
			case PromotionData.CurrencyType.Dollar:
			{
				return "$";
			}
			
			case PromotionData.CurrencyType.Pound:
			{
				return "\u00A3";
			}
			
			default:
			{
				return "\u20AC";
			}
		}
	}
}
}