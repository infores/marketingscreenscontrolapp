﻿//
// AdvertisementScreen.cs
//
// Created by Lazarevich Aleksey on 7.05.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

namespace Geec.Advertisement
{
[RequireComponent(typeof(Image))]
[RequireComponent(typeof(Mask))]
public abstract class AdvertisementScreen : MonoBehaviour 
{
	public AdvertisementViewConroller Controller;
	public Image ImageMask;
	
	
	protected bool SetupMask()
	{
		bool result = true;
	
		try
		{
			ImageMask.fillAmount = 0;
	
			var FillMethodValues = Enum.GetValues( typeof(Image.FillMethod) );
			ImageMask.fillMethod =  (Image.FillMethod)FillMethodValues.GetValue( UnityEngine.Random.Range( 0, FillMethodValues.Length ) );
		}
		catch( Exception ex )
		{
			Debug.LogError( "AdvertisementScreen::SetupMask(): " + ex.Message );
			result = false;
		}
		
		return result;
	}
	
	protected IEnumerator ShowScreen()
	{
		if( ImageMask != null )
		{
			while( ImageMask.fillAmount < 1 )
			{
				ImageMask.fillAmount += Time.deltaTime;
				yield return null;
			}
		}
		else
		{
			Debug.LogError( "AdvertisementScreen::ShowScreen(): ImageMask not set." );
		}
	}
	
	protected IEnumerator HideScreen()
	{
		if( ImageMask != null )
		{
			while( ImageMask.fillAmount > 0 )
			{
				ImageMask.fillAmount -= Time.deltaTime;
				yield return null;
			}
		}
		else
		{
			Debug.LogError( "AdvertisementScreen::HideScreen(): ImageMask not set." );
		}
	}
}
}