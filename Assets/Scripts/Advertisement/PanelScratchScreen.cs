//
// PanelScratch.cs
//
// Created by Lazarevich Aleksey on 28.04.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Geec.Advertisement
{
public class PanelScratchScreen : AdvertisementScreen 
{
	public Image ImageBackground;
	public Image ImageLogo;
	public List<ControlScratch> ScratchControls = new List<ControlScratch>(5);
	public Button ButtonExit;
	public Text TextButtonExit;
	public Text TextWonLost;
	
	ScratchData data;
	HashSet<int> correctAnswerIndexes = new HashSet<int>();
	HashSet<int> foundAnswerIndexes = new HashSet<int>();
	bool gameComplete;
	bool won;


	public bool Show( ScratchData data )
	{
		if( data == null )
		{
			return false;
		}
		
		this.data = data;
		
		if( !LoadImages() )
		{
			return false;
		}
		
		if( !SetupScratchControls() )
		{
			return false;
		}
		
		gameObject.SetActive( true );
		StartCoroutine( ShowScreen() );
		
		return true;
	}
	
	public void OnExitClick()
	{
		if( won &&
			data != null &&
			data.WinUrl != null )
		{
			Application.OpenURL( data.WinUrl );
		}
		
		StartCoroutine( Exit() );
	}
	
	IEnumerator Exit()
	{
		yield return StartCoroutine( HideScreen() );
		
		gameObject.SetActive( false );
		
		Controller.ScratchClosed( data, won );
	}
	
	bool SetupScratchControls()
	{
		if( ButtonExit == null )
		{
			Debug.LogWarning( "PanelScratch::SetupScratchControls(): ButtonExit not set." );
			return false;
		}
	
		if( TextButtonExit == null )
		{
			Debug.LogWarning( "PanelScratch::SetupScratchControls(): TextButtonExit not set." );
			return false;
		}
		
		if( TextWonLost == null )
		{
			Debug.LogWarning( "PanelScratch::SetupScratchControls(): TextWonLost not set." );
			return false;
		}
	
		for( int i=0; i<ScratchControls.Count; i++ )
		{
			if( ScratchControls[i] == null )
			{
				Debug.LogWarning( "PanelScratch::SetupScratchControls(): ControlScratch not set." );
				return false;
			}
		}
		
		Sprite correctAnswerSprite = null;
		List<Sprite> incorrectAnswerSprites = new List<Sprite>();
		
		while( correctAnswerIndexes.Count < 3 )
		{
			correctAnswerIndexes.Add( UnityEngine.Random.Range( 0, 5 ) );
		}
		
		int value = UnityEngine.Random.Range( 0, 3 );
		switch( value )
		{
			case 2:
			{
				correctAnswerSprite = ScratchControls[0].SpriteBackground1;
				incorrectAnswerSprites.Add( ScratchControls[0].SpriteBackground2 );
				incorrectAnswerSprites.Add( ScratchControls[0].SpriteBackground3 );
				break;
			}
			
			case 1:
			{
				correctAnswerSprite = ScratchControls[0].SpriteBackground2;
				incorrectAnswerSprites.Add( ScratchControls[0].SpriteBackground1 );
				incorrectAnswerSprites.Add( ScratchControls[0].SpriteBackground3 );
				break;
			}
			
			case 0:
			{
				correctAnswerSprite = ScratchControls[0].SpriteBackground3;
				incorrectAnswerSprites.Add( ScratchControls[0].SpriteBackground1 );
				incorrectAnswerSprites.Add( ScratchControls[0].SpriteBackground2 );
				break;
			}
		}
		
		if( correctAnswerSprite == null )
		{
			Debug.LogWarning( "PanelScratch::SetupScratchControls(): correctAnswerSprite not set." );
			return false;
		}
		
		for( int i=0; i<ScratchControls.Count; i++ )
		{
			if( correctAnswerIndexes.Contains(i) )
			{
				ScratchControls[i].ImageBackground.sprite = correctAnswerSprite;
			}
			else
			{
				if( incorrectAnswerSprites.Count != 0 )
				{
					ScratchControls[i].ImageBackground.sprite = incorrectAnswerSprites[0];
					incorrectAnswerSprites.RemoveAt(0);
				}
				else
				{
					Debug.LogWarning( "PanelScratch::SetupScratchControls(): incorrectAnswerSprites.Count == 0." );
					return false;
				}
			}
		}
		
//		вывод правильных ответов на консоль
//		foreach( int a in correctAnswerIndexes )
//		{
//			Debug.LogWarning( a );
//		}
	
		return true;
	}
	
	void OnEnable()
	{
		gameComplete = false;
		
		if( ButtonExit != null )
		{
			ButtonExit.gameObject.SetActive( false );
		}
		
		if( TextWonLost != null )
		{
			TextWonLost.rectTransform.localScale = Vector3.forward;
			TextWonLost.gameObject.SetActive( false );
		}
	}
	
	void OnDisable()
	{
		correctAnswerIndexes.Clear();
		foundAnswerIndexes.Clear();
	}
	
	public void OnScratch( ControlScratch control )
	{
		if( control == null )
		{
			return;
		}
	
		if( gameComplete )
		{
			return;
		}
		
		int index = ScratchControls.IndexOf( control );
		if( correctAnswerIndexes.Contains( index ) )
		{
			foundAnswerIndexes.Add( index );
			
			if( foundAnswerIndexes.Count == correctAnswerIndexes.Count )
			{
				won = true;
				gameComplete = true;
			}
		}
		else
		{
			won = false;
			gameComplete = true;
		}
		
		if( gameComplete )
		{
			StartCoroutine( EndGame() );
		}
	}
	
	IEnumerator EndGame()
	{
		TextWonLost.gameObject.SetActive( true );
		
		if( won )
		{
			TextButtonExit.text = "Visit";
			TextWonLost.text = data.WonMessage;
			TextWonLost.color = data.WonMessageColor;
			TextWonLost.fontStyle = FontStyle.Bold;
			
			yield return StartCoroutine( TextWonAnimation() );
		}
		else
		{
			TextButtonExit.text = "Exit";
			TextWonLost.text = data.LostMessage;
			TextWonLost.color = data.LostMessageColor;
			TextWonLost.fontStyle = FontStyle.Italic;
			
			yield return StartCoroutine( TextLostAnimation() );
		}
		
		ButtonExit.gameObject.SetActive( true );
		
		yield return null;
	}
	
	IEnumerator TextWonAnimation()
	{
		RectTransform rectTransform = TextWonLost.rectTransform;
		bool scale = true;
		bool rotation = true;
		Vector3 rectTransformEulerAngles = rectTransform.localRotation.eulerAngles;
		Vector3 zRotation = new Vector3(0, 0, 720);
		
		while( scale || rotation )
		{
			if( scale )
			{
				if( rectTransform.localScale != Vector3.one )
				{
					rectTransform.localScale = Vector3.MoveTowards( rectTransform.localScale, Vector3.one, Time.deltaTime );
				}
				else
				{
					scale = false;
				}
			}
			
			if( rotation )
			{
				if( rectTransformEulerAngles != zRotation )
				{
					rectTransformEulerAngles = Vector3.MoveTowards( rectTransformEulerAngles,
																	zRotation, 
																	Time.deltaTime*500 );
																					
					rectTransform.localRotation = Quaternion.Euler( rectTransformEulerAngles );
				}
				else
				{
					rotation = false;
				}
			}
			
			yield return null;	
		}
	}
	
	IEnumerator TextLostAnimation()
	{
		Vector3 newScale = new Vector3( 1, 1.5f, 1 );
		
		while( TextWonLost.rectTransform.localScale != newScale )
		{
			TextWonLost.rectTransform.localScale = Vector3.MoveTowards( TextWonLost.rectTransform.localScale, 
																		newScale, Time.deltaTime );
																		
			yield return null;	
		}
	}
	
	bool LoadImages()
	{
		bool result = true;
		
		try
		{
			Sprite sprite = Controller.LoadSprite( data.BackgroundImageName );
			if( sprite == null )
			{
				return false;
			}
			else
			{
				ImageBackground.sprite = sprite;
			}
			
			sprite = Controller.LoadSprite( data.LogoImageName, ImageType.Logo );
			if( sprite == null )
			{
				return false;
			}
			else
			{
				ImageLogo.sprite = sprite;
			}
		}
		catch( Exception ex )
		{
			Debug.LogError( "PanelScratch::LoadImages(): " + ex.Message );
			result = false;
		}
		
		return result;
	}
}
}