//
// AdvertisementViewConroller.cs
//
// Created by Lazarevich Aleksey on 28.04.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using System.Collections;

namespace Geec.Advertisement
{
public class AdvertisementViewConroller : MonoBehaviour 
{
	public AdvertisementController Controller;
	public PanelPromotionScreen PanelPromotion;
	public PanelQuizScreen PanelQuiz;
	public GameObject PanelQuizVictory;
	public PanelScratchScreen PanelScratch;
	public PanelNotification PanelNotification;

	protected const string pathBackgrounds = "Advertisement/Sprites/Backgrounds/";
	protected const string pathGoods = "Advertisement/Sprites/Goods/";
	protected const string pathSale = "Advertisement/Sprites/Sale/";
	protected const string pathLogo = "Advertisement/Sprites/Logo/";
		

	public bool ShowPromotion( PromotionData data, bool reminder = false )
	{
		return PanelPromotion.Show( data, reminder );
	}
	
	public void PromotionClosed( PromotionData promotionData = null, bool remind = false )
	{
		Controller.PromotionClosed( promotionData, remind );
	}
	
	public bool ShowQuiz( QuizData data )
	{
		return PanelQuiz.Show( data );
	}
	
	public void QuizClosed( QuizData data, int answerIndex )
	{
		Controller.QuizClosed( data, answerIndex );
	}
	
	public void ShowQuizVictoryPopup()
	{
		PanelNotification.ShowQuizVictoryPopup();
	}
	
	public void OnQuizVictoryPopupClick()
	{
		if( PanelQuizVictory != null )
		{
			PanelQuizVictory.SetActive( true );
		}
		
		Controller.QuizVicroryPopupShown();
	}
	
	public bool ShowScratch( ScratchData data )
	{
		return PanelScratch.Show( data );
	}
	
	public void ScratchClosed( ScratchData data, bool victory)
	{
		Controller.ScratchClosed( data, victory );
	}
	
	virtual public Sprite LoadSprite( string pathName, ImageType type = ImageType.Background )
	{
		switch( type )
		{
			case ImageType.Background:
			{
				pathName = pathBackgrounds + pathName;
				break;
			}
			
			case ImageType.Goods:
			{
				pathName = pathGoods + pathName;
				break;
			}
			
			case ImageType.Logo:
			{
				pathName = pathLogo + pathName;
				break;
			}
			
			case ImageType.Sale:
			{
				pathName = pathSale + pathName;
				break;
			}
		}
		
		return Resources.Load<Sprite>( pathName ) as Sprite;
	}
	
	public void QuizVictoryClosed()
	{
		Controller.QuizVictoryClosed();
	}
}
}