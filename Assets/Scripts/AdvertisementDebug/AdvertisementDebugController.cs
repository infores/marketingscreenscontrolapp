﻿//
// AdvertisementDebugController.cs
//
// Created by Lazarevich Aleksey on 10.06.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using System.Collections;

namespace Geec.Advertisement
{
public class AdvertisementDebugController : MonoBehaviour, IAdvertisementControllerListener
{
	public AdvertisementDebugViewController View;
	public AdvertisementController Controller;
	public AdvertisementConfigurator Configurator;
	
	void Start()
	{
		Controller.SetListener( this );
	
		View.Show();
	}
	
	public void ShowAdvertisement()
	{
		View.Hide();
		Controller.ShowAdvertisement();
	}
	
	public void ResetRegistry()
	{
		Configurator.ResetRegistry();
	}

	#region IAdvertisementControllerListener implementation

	public void AdvertisementClosed ()
	{
		View.Show();
	}

	#endregion
}
}