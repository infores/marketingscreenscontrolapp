﻿//
// AdvertisementDebugViewController.cs
//
// Created by Lazarevich Aleksey on 10.06.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using System.Collections;

namespace Geec.Advertisement
{
public class AdvertisementDebugViewController : MonoBehaviour 
{
	public AdvertisementDebugController Controller;
	public Canvas Canvas;
	
	
	public void Hide()
	{
		Canvas.enabled = false;
	}
	
	public void Show()
	{
		Canvas.enabled = true;
	}
	
	public void OnShowAdvertisementClick()
	{
		Controller.ShowAdvertisement();
	}
	
	public void OnResetRegistryClick()
	{
		Controller.ResetRegistry();
	}
}
}