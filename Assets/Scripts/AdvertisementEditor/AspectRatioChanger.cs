﻿//
// AspectRatioChanger.cs
//
// Created by Berezin Boris on 13.07.2015.
// Copyright (c) 2015 Berezin Boris. All rights reserved.
//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Geec.Advertisement;

namespace Geec.Advertisement.EditorApplication
{
	public enum ScreenAspect
	{
		Screen4_3,
		Screen16_10,
		Screen16_9,
		Screen9_16,
		Screen10_16,
		Screen3_4
	}
	public class AspectRatioChanger : MonoBehaviour, IAdvertisementControllerListener
	{
		[SerializeField] private Image ImageButton4_3 = null;
		[SerializeField] private Image ImageButton16_10 = null;
		[SerializeField] private Image ImageButton16_9 = null;
		[SerializeField] private Image ImageButton9_16 = null;
		[SerializeField] private Image ImageButton10_16 = null;
		[SerializeField] private Image ImageButton3_4 = null;
		[SerializeField] private RectTransform rectTransformPromotionPanel = null;
		[SerializeField] private RectTransform rectTransformQuizesPanel = null;
		[SerializeField] private RectTransform rectTransformQuizVictotyPanel = null;
		[SerializeField] private RectTransform rectTransformScratchPanel = null;
		[SerializeField] private CanvasScaler canvasScaler = null;
		public ScreenAspect CurrentScreenAspect = ScreenAspect.Screen4_3;
		private IAdvertisementController advController; 	

		private	void Awake() 
		{ 
			advController = AdvertisementController.Instance(); 
			if (advController != null) 
			{ 
				advController.SetListener( this ); 
			} 
		} 

		private void Start()
		{
			SetAspectRatio4_3();		
		}

		public void AdvertisementClosed()
		{
			Debug.LogWarning("AdvertisementClosed()");
			ReturnScreenResolution();
		}

		private void ResetAllButtons() 
		{
			ImageButton4_3.color = Color.gray;
			ImageButton16_10.color = Color.gray;
			ImageButton16_9.color = Color.gray;
			ImageButton9_16.color = Color.gray;
			ImageButton10_16.color = Color.gray;
			ImageButton3_4.color = Color.gray;
		}
		
		public void SetAspectRatio4_3() 
		{
			CurrentScreenAspect = ScreenAspect.Screen4_3;
			ResetAllButtons();
			ImageButton4_3.color = Color.white;
		}
		public void SetAspectRatio16_9() 
		{
			CurrentScreenAspect = ScreenAspect.Screen16_9;
			ResetAllButtons();
			ImageButton16_9.color = Color.white;
		}
		public void SetAspectRatio16_10() 
		{
			CurrentScreenAspect = ScreenAspect.Screen16_10;
			ResetAllButtons();
			ImageButton16_10.color = Color.white;
		}
		public void SetAspectRatio9_16() 
		{
			CurrentScreenAspect = ScreenAspect.Screen9_16;
			ResetAllButtons();
			ImageButton9_16.color = Color.white;
		}
		public void SetAspectRatio10_16() 
		{
			CurrentScreenAspect = ScreenAspect.Screen10_16;
			ResetAllButtons();
			ImageButton10_16.color = Color.white;
		}
		public void SetAspectRatio3_4() 
		{
			CurrentScreenAspect = ScreenAspect.Screen3_4;
			ResetAllButtons();
			ImageButton3_4.color = Color.white;
		}

		public void ReturnScreenResolution()
		{
			rectTransformPromotionPanel.sizeDelta = new Vector2(0, 0);
			rectTransformQuizesPanel.sizeDelta = new Vector2(0, 0);
			rectTransformQuizVictotyPanel.sizeDelta = new Vector2(0, 0);
			rectTransformScratchPanel.sizeDelta = new Vector2(0, 0);
			canvasScaler.referenceResolution = new Vector2(2048, 1536);
		}

		public void SetScreenResolution()
		{
			switch (CurrentScreenAspect)
			{
			case ScreenAspect.Screen4_3:
			{
				rectTransformPromotionPanel.sizeDelta = new Vector2(0, 0);
				rectTransformQuizesPanel.sizeDelta = new Vector2(0, 0);
				rectTransformQuizVictotyPanel.sizeDelta = new Vector2(0, 0);
				rectTransformScratchPanel.sizeDelta = new Vector2(0, 0);
				canvasScaler.referenceResolution = new Vector2(2048, 1536);
				break;
			}
			case ScreenAspect.Screen16_10:
			{
				rectTransformPromotionPanel.sizeDelta = new Vector2(0 ,-256);
				rectTransformQuizesPanel.sizeDelta = new Vector2(0, -256);
				rectTransformQuizVictotyPanel.sizeDelta = new Vector2(0, -256);
				rectTransformScratchPanel.sizeDelta = new Vector2(0, -256);
				canvasScaler.referenceResolution = new Vector2(2048, 1806);
				break;
			}
			case ScreenAspect.Screen16_9:
			{
				rectTransformPromotionPanel.sizeDelta = new Vector2(0 ,-384);
				rectTransformQuizesPanel.sizeDelta = new Vector2(0, -384);
				rectTransformQuizVictotyPanel.sizeDelta = new Vector2(0, -384);
				rectTransformScratchPanel.sizeDelta = new Vector2(0, -384);
				canvasScaler.referenceResolution = new Vector2(2048, 1912);
				break;
			}
			case ScreenAspect.Screen9_16:
			{
				rectTransformPromotionPanel.sizeDelta = new Vector2(-1836 ,0);
				rectTransformQuizesPanel.sizeDelta = new Vector2(-1836, 0);
				rectTransformQuizVictotyPanel.sizeDelta = new Vector2(-1836, 0);
				rectTransformScratchPanel.sizeDelta = new Vector2(-1836, 0);
				canvasScaler.referenceResolution = new Vector2(2048, 3666);
				break;
			}
			case ScreenAspect.Screen10_16:
			{
				rectTransformPromotionPanel.sizeDelta = new Vector2(-1580,0);
				rectTransformQuizesPanel.sizeDelta = new Vector2(-1580, 0);
				rectTransformQuizVictotyPanel.sizeDelta = new Vector2(-1580, 0);
				rectTransformScratchPanel.sizeDelta = new Vector2(-1580, 0);
				canvasScaler.referenceResolution = new Vector2(2048, 3280);
				break;
			}
			case ScreenAspect.Screen3_4:
			{
				rectTransformPromotionPanel.sizeDelta = new Vector2(-1200 ,0);
				rectTransformQuizesPanel.sizeDelta = new Vector2(-1200, 0);
				rectTransformQuizVictotyPanel.sizeDelta = new Vector2(-1200, 0);
				rectTransformScratchPanel.sizeDelta = new Vector2(-1200, 0);
				canvasScaler.referenceResolution = new Vector2(2048, 2734);
				break;
			}
			}
		}
	}
}
