﻿//
// LayoutButtons.cs
//
// Created by Berezin Boris on 13.07.2015.
// Copyright (c) 2015 Berezin Boris. All rights reserved.
//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Geec.Advertisement;
using Geec.Advertisement.EditorApplication;

namespace Geec.Advertisement.EditorApplication
{
	internal enum LayOut
	{
		Goods,
		Sale,
		Text
	}

	public class LayoutButtons : MonoBehaviour 
	{
		[SerializeField] private AdvertisementEditorPanelConfiguration advertisementEditorPanelConfiguration = null;
		[SerializeField] private LayOut layout = LayOut.Goods;
		[SerializeField] private Number number = null;
		[SerializeField] private Image middleImage = null;
		[SerializeField] private Image middleLeftImage = null;
		[SerializeField] private Image middleRightImage = null;
		[SerializeField] private Image topLeftImage = null;
		[SerializeField] private Image topMiddleImage = null;
		[SerializeField] private Image topRightImage = null;
		[SerializeField] private Image bottomLeftImage = null;
		[SerializeField] private Image bottomRightImage = null;
		[SerializeField] private Image bottomMiddleImage = null;

		private void ResetAllButtons()
		{
			middleImage.color = Color.gray;
			middleLeftImage.color = Color.gray;
			middleRightImage.color = Color.gray;
			topLeftImage.color = Color.gray;
			topMiddleImage.color = Color.gray;
			topRightImage.color = Color.gray;
			bottomLeftImage.color = Color.gray;
			bottomRightImage.color = Color.gray;
			bottomMiddleImage.color = Color.gray;
		}
		
		public void Middle () 
		{
			ResetAllButtons();
			middleImage.color = Color.white;
			switch (layout)
			{
			case LayOut.Goods:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].GoodsLayout = PromotionData.PromotionLayout.Middle;
				break;
			}
			case LayOut.Sale:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].SaleLayout = PromotionData.PromotionLayout.Middle;
				break;
			}
			case LayOut.Text:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].TextLayout = PromotionData.PromotionLayout.Middle;
				break;
			}
			}
		}

		public void MiddleLeft () 
		{
			ResetAllButtons();
			middleLeftImage.color = Color.white;
			switch (layout)
			{
			case LayOut.Goods:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].GoodsLayout = PromotionData.PromotionLayout.MiddleLeft;
				break;
			}
			case LayOut.Sale:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].SaleLayout = PromotionData.PromotionLayout.MiddleLeft;
				break;
			}
			case LayOut.Text:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].TextLayout = PromotionData.PromotionLayout.MiddleLeft;
				break;
			}
			}
		}

		public void MiddleRight () 
		{
			ResetAllButtons();
			middleRightImage.color = Color.white;
			switch (layout)
			{
			case LayOut.Goods:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].GoodsLayout = PromotionData.PromotionLayout.MiddleRight;
				break;
			}
			case LayOut.Sale:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].SaleLayout = PromotionData.PromotionLayout.MiddleRight;
				break;
			}
			case LayOut.Text:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].TextLayout = PromotionData.PromotionLayout.MiddleRight;
				break;
			}
			}
		}

		public void TopMiddle () 
		{
			ResetAllButtons();
			topMiddleImage.color = Color.white;
			switch (layout)
			{
			case LayOut.Goods:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].GoodsLayout = PromotionData.PromotionLayout.TopMiddle;
				break;
			}
			case LayOut.Sale:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].SaleLayout = PromotionData.PromotionLayout.TopMiddle;
				break;
			}
			case LayOut.Text:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].TextLayout = PromotionData.PromotionLayout.TopMiddle;
				break;
			}
			}
		}

		public void TopLeft () 
		{
			ResetAllButtons();
			topLeftImage.color = Color.white;
			switch (layout)
			{
			case LayOut.Goods:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].GoodsLayout = PromotionData.PromotionLayout.TopLeft;
				break;
			}
			case LayOut.Sale:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].SaleLayout = PromotionData.PromotionLayout.TopLeft;
				break;
			}
			case LayOut.Text:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].TextLayout = PromotionData.PromotionLayout.TopLeft;
				break;
			}
			}
		}

		public void TopRight () 
		{
			ResetAllButtons();
			topRightImage.color = Color.white;
			switch (layout)
			{
			case LayOut.Goods:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].GoodsLayout = PromotionData.PromotionLayout.TopRight;
				break;
			}
			case LayOut.Sale:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].SaleLayout = PromotionData.PromotionLayout.TopRight;
				break;
			}
			case LayOut.Text:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].TextLayout = PromotionData.PromotionLayout.TopRight;
				break;
			}
			}
		}

		public void ButtomMiddle () 
		{
			ResetAllButtons();
			bottomMiddleImage.color = Color.white;
			switch (layout)
			{
			case LayOut.Goods:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].GoodsLayout = PromotionData.PromotionLayout.BottomMiddle;
				break;
			}
			case LayOut.Sale:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].SaleLayout = PromotionData.PromotionLayout.BottomMiddle;
				break;
			}
			case LayOut.Text:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].TextLayout = PromotionData.PromotionLayout.BottomMiddle;
				break;
			}
			}
		}

		public void ButtomLeft () 
		{
			ResetAllButtons();
			bottomLeftImage.color = Color.white;
			switch (layout)
			{
			case LayOut.Goods:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].GoodsLayout = PromotionData.PromotionLayout.BottomLeft;
				break;
			}
			case LayOut.Sale:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].SaleLayout = PromotionData.PromotionLayout.BottomLeft;
				break;
			}
			case LayOut.Text:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].TextLayout = PromotionData.PromotionLayout.BottomLeft;
				break;
			}
			}
		}

		public void ButtomRight () 
		{
			ResetAllButtons();
			bottomRightImage.color = Color.white;
			switch (layout)
			{
			case LayOut.Goods:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].GoodsLayout = PromotionData.PromotionLayout.BottomRight;
				break;
			}
			case LayOut.Sale:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].SaleLayout = PromotionData.PromotionLayout.BottomRight;
				break;
			}
			case LayOut.Text:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].TextLayout = PromotionData.PromotionLayout.BottomRight;
				break;
			}
			}
		}
	}
}
