﻿//
// Symbols.cs
//
// Created by Berezin Boris on 13.07.2015.
// Copyright (c) 2015 Berezin Boris. All rights reserved.
//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;


namespace Geec.Advertisement.EditorApplication
{
internal enum Symbol
{
	Euro,
	Pound
}

[ExecuteInEditMode]
public class Symbols : MonoBehaviour 
{
	[SerializeField] private Text text = null;
	[SerializeField] private Symbol symbol = Symbol.Euro;
	
	
	private void Awake() 
	{
		text.text = "\u00A3";
		switch(symbol)
		{
			case Symbol.Euro:
			{
				text.text = "\u20AC";
				break;
			}
				
			case Symbol.Pound:
			{
				text.text = "\u00A3";
				break;
			}		
		}
	}
}
}
