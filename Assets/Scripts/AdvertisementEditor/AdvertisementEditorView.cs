﻿//
// AdvertisementEditorView.cs
//
// Created by Lazarevich Aleksey on 28.06.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//


using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Geec.Advertisement.EditorApplication
{
public class AdvertisementEditorView : AdvertisementViewConroller 
{
	public AdvertisementEditorController EditorController;
	public AdvertisementEditorPanelEditor PanelEditor;
	[SerializeField] private AspectRatioChanger aspectRatioChanger = null;
	
	Dictionary<ImageType, List<Texture2D>> images;
		

	public void SetImages( Dictionary<ImageType, List<Texture2D>> images )
	{
		this.images = images;
		
		PanelEditor.OnLoaded();
	}
	
	public void OnLoadingErrors( List<string> errors = null )
	{
		PanelEditor.OnLoadingErrors( errors );
	}

	public void ParseData( AdvertisementData data )
	{
		if( data == null )
		{
			Debug.LogError( "AdvertisementEditorView::ParseData(): data is null." );
			return;
		}
		
		PanelEditor.ParseData( data );
	}
	
	public void UpdateData( AdvertisementData data )
	{
		EditorController.UpdateData( data );
	}

	public void SaveConfig()
	{
		EditorController.SaveConfig();
	}
	
	
	public void Test( AdvertisementType type, int index )
	{
		PanelEditor.gameObject.SetActive( false );
		//TODO: change aspect ratio here
		aspectRatioChanger.SetScreenResolution();
		EditorController.ShowAdvertisement( type, index );
	}
	
	public void SwitchToEditor()
	{
		//TODO: return aspect ratio here
		PanelEditor.gameObject.SetActive( true );
		aspectRatioChanger.ReturnScreenResolution();
	}
	
	override public Sprite LoadSprite( string pathName, ImageType type = ImageType.Background )
	{
		if( images == null )
		{
			Debug.LogError( "AdvertisementEditorView::LoadSprite(): images is null." );
			return null;
		}
	
		Sprite result = null;
		
		List<Texture2D> textures = images[type];
	
		foreach( Texture2D texture in textures )
		{
			if( texture.name == pathName )
			{
				result = Sprite.Create( texture, new Rect( 0, 0, texture.width, texture.height ), new Vector2( 0.5f, 0.5f ) );
				break;
			}
		}
	
		return result;
	}
}
}
