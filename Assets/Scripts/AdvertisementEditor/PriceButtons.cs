﻿//
// PriceButtons.cs
//
// Created by Berezin Boris on 13.07.2015.
// Copyright (c) 2015 Berezin Boris. All rights reserved.
//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Geec.Advertisement;
using Geec.Advertisement.EditorApplication;
namespace Geec.Advertisement.EditorApplication
{
	internal enum ToggleType
	{
		Price,
		RightAnswer
	}

	public class PriceButtons : MonoBehaviour 
	{
		[SerializeField] private AdvertisementEditorPanelConfiguration advertisementEditorPanelConfiguration = null;
		[SerializeField] private ToggleType toggleType = ToggleType.Price;
		[SerializeField] private Number number = null;
		[SerializeField] private Image ImageDoll = null;
		[SerializeField] private Image ImageEuro = null;
		[SerializeField] private Image ImagePound = null;

		private void ResetAllButtons()
		{
			ImageDoll.color = Color.gray;
			ImageEuro.color = Color.gray;
			ImagePound.color = Color.gray;
		}

		public void SetDoll()
		{
			ResetAllButtons();
			ImageDoll.color = Color.white;
			switch (toggleType)
			{
			case ToggleType.Price:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].Currency = PromotionData.CurrencyType.Dollar;
				break;
			}
			case ToggleType.RightAnswer:
			{
				advertisementEditorPanelConfiguration.AdvData.Quizes[number.CurrentNumber].Question.CorrectAnswer = 0;
				break;
			}
			}
		}

		public void SetEuro()
		{
			ResetAllButtons();
			ImageEuro.color = Color.white;
			switch (toggleType)
			{
			case ToggleType.Price:
			{
			advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].Currency = PromotionData.CurrencyType.Euro;
				break;
			}
			case ToggleType.RightAnswer:
			{
				advertisementEditorPanelConfiguration.AdvData.Quizes[number.CurrentNumber].Question.CorrectAnswer = 1;
				break;
			}
			}
		}

		public void SetPound()
		{
			ResetAllButtons();
			ImagePound.color = Color.white;
			switch (toggleType)
			{
			case ToggleType.Price:
			{
			advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].Currency = PromotionData.CurrencyType.Pound;
				break;
			}
			case ToggleType.RightAnswer:
			{
				advertisementEditorPanelConfiguration.AdvData.Quizes[number.CurrentNumber].Question.CorrectAnswer = 2;
				break;
			}
			}
		}
	}
}
