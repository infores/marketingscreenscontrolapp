﻿//
// TabController.cs
//
// Created by Berezin Boris on 13.07.2015.
// Copyright (c) 2015 Berezin Boris. All rights reserved.
//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using Geec.Advertisement;
using Geec.Advertisement.EditorApplication;

namespace Geec.Advertisement.EditorApplication
{
	public class TabController : MonoBehaviour 
	{
		[SerializeField] private AdvertisementEditorPanelConfiguration advertisementEditorPanelConfiguration = null;
		[SerializeField] private List<GameObject> tabPanels = new List<GameObject>();
		[SerializeField] private List<Text> buttonsText = new List<Text>();
		
		void Start()
		{
			ChangePanel( 0 );
		}

		public void ChangePanel(int id)
		{
			int i = 0;
			foreach (var panel in tabPanels)
			{
				panel.SetActive(i == id);
				i++;
				if (id == 0)
					advertisementEditorPanelConfiguration.advertisementType = AdvertisementType.Promotion;
				if (id == 1)
					advertisementEditorPanelConfiguration.advertisementType = AdvertisementType.Quiz;
				if (id == 2)
					advertisementEditorPanelConfiguration.advertisementType = AdvertisementType.Scratch;
			}
			
			foreach( Text text in buttonsText )
			{
				text.color = Color.gray;
			}
			
			if( id < buttonsText.Count &&
				id >= 0 )
			{
				buttonsText[id].color = Color.black;
			}
		}
	}
}


