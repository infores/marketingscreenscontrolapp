﻿//
// Symbols.cs
//
// Created by Berezin Boris on 13.07.2015.
// Copyright (c) 2015 Berezin Boris. All rights reserved.
//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Geec.Advertisement;
using Geec.Advertisement.EditorApplication;
namespace Geec.Advertisement.EditorApplication
{
	internal enum SliderType
	{
		SaleAmount,
		SaleRotation,
		SkipTime,
		PromotionChance,
		QiizChance,
		ScratchChance
	}

	public class SliderField : MonoBehaviour 
	{
		[SerializeField] private AdvertisementEditorPanelConfiguration advertisementEditorPanelConfiguration = null;
		[SerializeField] private SliderType sliderType = SliderType.SaleAmount;
		[SerializeField] private Number number = null;
		[SerializeField] private Slider slider = null;
		[SerializeField] private Text text = null;
		[SerializeField] private string addingText = "%";

		public void ValueChanged()
		{
			text.text = slider.value.ToString("f0") + addingText;

			switch (sliderType)
			{
			case SliderType.PromotionChance:
			{
				advertisementEditorPanelConfiguration.AdvData.PromotionChance = (int)slider.value; 
				break;
			}
			case SliderType.QiizChance:
			{
				advertisementEditorPanelConfiguration.AdvData.QuizChance = (int)slider.value; 
				break;
			}
			case SliderType.ScratchChance:
			{
				advertisementEditorPanelConfiguration.AdvData.ScratchChance = (int)slider.value; 
				break;
			}
			case SliderType.SaleAmount:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].SaleAmount = (int)slider.value; 
				break;
			}
			case SliderType.SaleRotation:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].SaleRotation = (int)slider.value;
				break;
			}
			case SliderType.SkipTime:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].SkipTime = (int)slider.value;
				break;
			}		
			}
		}
	}
}
