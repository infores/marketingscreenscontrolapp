﻿//
// AdvertisementEditorPanelEditor.cs
//
// Created by Lazarevich Aleksey on 30.06.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//


using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;


namespace Geec.Advertisement.EditorApplication
{
public class AdvertisementEditorPanelNotification : MonoBehaviour 
{
	public Text TextErrors;
	public Text TextLoading;
	public Text TextRestart;
	
	void Awake()
	{
		TextErrors.enabled = false;
		TextRestart.enabled = false;
	}
	
	public void SetErrors( List<string> errors = null )
	{
		TextErrors.enabled = true;
		TextLoading.enabled = false;
		TextRestart.enabled = true;
		
		if( errors != null )
		{
			foreach( string str in errors )
			{
				TextErrors.text = TextErrors.text.Insert( TextErrors.text.Length, str );
				TextErrors.text += "\n";
			}
		}
	}
}
}