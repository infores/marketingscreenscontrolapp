﻿//
// EditorController.cs
//
// Created by Lazarevich Aleksey on 28.06.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Geec.Advertisement;
using Geec.Utils;

namespace Geec.Advertisement.EditorApplication
{
public class AdvertisementEditorController : AdvertisementController
{
	public AdvertisementEditorView EditorView;
	public AdvertisementEditorImagesLoader ImagesLoader;
	
	public const string ConfigFolder = "Configuration";
	const string configFilePath = "Configuration/config.xml";
	
	
	void Awake()
	{	
		if( Application.isPlaying )
		{
			data = Utils.FileUtils.DeserializeFromXml<AdvertisementData>( Path.Combine( Application.dataPath, 
			                                                                           configFilePath ) );
			
			if( data == null )
			{
				data = new AdvertisementData();
			}
		}
	}
	
	void Start()
	{
		if( Application.isPlaying )
		{
			EditorView.ParseData( data );
			
			ImagesLoader.LoadImages();
		}
	}
	
	
	public void ImagesLoaded( Dictionary<ImageType, List<Texture2D>> images, List<string> errors )
	{
		if( images == null &&
			errors == null )
		{
			EditorView.OnLoadingErrors();
			return;
		}
		
		if( images.Count == 0 ||
			errors.Count > 0 )
		{
			EditorView.OnLoadingErrors( errors );
			return;
		}
	
		EditorView.SetImages( images );
	}

	
	public void UpdateData( AdvertisementData data )
	{
		if( data == null )
		{
			Debug.LogError( "AdvertisementEditorController::UpdateData(): data is null." );
			return;
		}
	
		this.data = data;		
	}

	public void SaveConfig()
	{
		FileUtils.SerializeToXml<AdvertisementData>( data, Path.Combine( Application.dataPath, configFilePath ) );
	}
	
	
	public void ShowAdvertisement( AdvertisementType type, int index )
	{
		if( data == null )
		{
			Debug.LogError( "AdvertisementEditorController::ShowAdvertisement(): data is null." );
			return;
		}
	
		switch( type )
		{
			case AdvertisementType.Promotion:
			{
				if( index < 0 || 
					index>=data.Promotions.Count )
				{
					Debug.LogError( "AdvertisementEditorController::ShowAdvertisement(): index out of range." );
				}
				else
				{
					if( !EditorView.ShowPromotion( data.Promotions[index] ) )
					{
						EditorView.SwitchToEditor();
					}
				}
				break;
			}
			
			case AdvertisementType.Quiz:
			{
				if( index < 0 || 
					index>=data.Quizes.Count )
				{
					Debug.LogError( "AdvertisementEditorController::ShowAdvertisement(): index out of range." );
				}
				else
				{
					if( !EditorView.ShowQuiz( data.Quizes[index] ) )
					{
						EditorView.SwitchToEditor();
					}
				}
				break;
			}
			
			case AdvertisementType.Scratch:
			{
				if( !EditorView.ShowScratch( data.ScratchData ) )
				{
					EditorView.SwitchToEditor();
				}
				break;
			}
		}
	}
	
	
	override public void QuizClosed( QuizData quiz, int answerIndex )
	{
		int index = data.Quizes.IndexOf( quiz );
		if( index != -1 )
		{
			if( data.Quizes[index].Question.CorrectAnswer == answerIndex )
			{				
				View.OnQuizVictoryPopupClick();
			}
			else
			{
				EditorView.SwitchToEditor();
			}
		}
		else
		{
			EditorView.SwitchToEditor();
		}
	}
	
	override public void ScratchClosed( ScratchData scratch, bool victory )
	{
		EditorView.SwitchToEditor();
	}
	
	override public void PromotionClosed( PromotionData promotion = null, bool remind = false )
	{
		if( remind &&
		   promotion != null )
		{
			View.ShowPromotion( promotion, true );
		}
		else
		{
			EditorView.SwitchToEditor();
		}
	}
	
	override public void QuizVicroryPopupShown()
	{
		// чтобы не срабатывала логика сохранения в родителе
	}
	
	override public void QuizVictoryClosed()
	{
		EditorView.SwitchToEditor();
	}
}
}