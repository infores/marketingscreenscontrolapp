﻿//
// SelectColor.cs
//
// Created by Berezin Boris on 13.07.2015.
// Copyright (c) 2015 Berezin Boris. All rights reserved.
//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Geec.Advertisement.EditorApplication;
namespace Geec.Advertisement.EditorApplication
{
	internal enum SelectColorType
	{
		Question,
		Answer1,
		Answer2,
		Answer3,
		ScratchWon,
		ScratchLost
	}

	public class SelectColor : MonoBehaviour 
	{
		[SerializeField] private AdvertisementEditorPanelConfiguration advertisementEditorPanelConfiguration = null;
		[SerializeField] private Number number = null;
		[SerializeField] private SelectColorType selectColorType = SelectColorType.Question;
		[SerializeField] private GameObject selectColorCanvas = null;
		public Color GettingColor = Color.white;
		public Image buttonImage = null;

		public void ColorSelect()
		{
			GameObject selectColorWindow = Instantiate (selectColorCanvas, Vector3.zero, Quaternion.identity) as GameObject;
			selectColorWindow.GetComponentInChildren<ColorSelector>().selectColor = this;
			selectColorWindow.GetComponentInChildren<ColorSelector>().SetColor(GettingColor);
		}

		public void SetColor()
		{
			switch (selectColorType)
			{
			case SelectColorType.Question:
			{
				advertisementEditorPanelConfiguration.AdvData.Quizes[number.CurrentNumber].Question.QuestionTextColor = GettingColor;
				break;
			}
			case SelectColorType.Answer1:
			{
				advertisementEditorPanelConfiguration.AdvData.Quizes[number.CurrentNumber].Question.Answer1Color = GettingColor;
				break;
			}
			case SelectColorType.Answer2:
			{
				advertisementEditorPanelConfiguration.AdvData.Quizes[number.CurrentNumber].Question.Answer2Color = GettingColor;
				break;
			}
			case SelectColorType.Answer3:
			{
				advertisementEditorPanelConfiguration.AdvData.Quizes[number.CurrentNumber].Question.Answer3Color = GettingColor;
				break;
			}
			case SelectColorType.ScratchWon:
			{
				advertisementEditorPanelConfiguration.AdvData.ScratchData.WonMessageColor = GettingColor;
				break;
			}
			case SelectColorType.ScratchLost:
			{
				advertisementEditorPanelConfiguration.AdvData.ScratchData.LostMessageColor = GettingColor;
				break;
			}
			}
		}
	}
}
