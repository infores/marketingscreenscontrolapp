﻿//
// AdvertisementEditorPanelConfiguration.cs
//
// Created by Lazarevich Aleksey on 28.06.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//


using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.IO;
using Geec.Advertisement;

namespace Geec.Advertisement.EditorApplication
{
public class AdvertisementEditorPanelConfiguration : MonoBehaviour 
{
	public AdvertisementEditorPanelEditor PanelEditor;
	public AdvertisementData AdvData = null;
	
	[SerializeField] private Number numberPromotion = null;
	[SerializeField] private Number numberQuizes = null;	
	[SerializeField] private Slider sliderPromotionChance = null;
	[SerializeField] private Text textPromotionChance = null;
	[SerializeField] private Slider sliderQuizChance = null;
	[SerializeField] private Text textQuizChance = null;
	[SerializeField] private Slider sliderScratchChance = null;
	[SerializeField] private Text textScratchChance = null;
	[SerializeField] private Slider sliderSaleAmount = null;
	[SerializeField] private Text textSaleAmount = null;
	[SerializeField] private Slider sliderSaleRotation = null;
	[SerializeField] private Text textSaleRotation = null;
	[SerializeField] private Slider sliderSkipTime = null;
	[SerializeField] private Text textSkipTime = null;
	[SerializeField] private LayoutButtons layoutButtonsGoods = null;
	[SerializeField] private LayoutButtons layoutButtonsSale = null;
	[SerializeField] private LayoutButtons layoutButtonsText = null;
	[SerializeField] private DescriptionField descriptionField = null;
	[SerializeField] private DescriptionField descriptionFieldLink = null;
	[SerializeField] private DescriptionField descriptionPrice = null;
	[SerializeField] private DescriptionField descriptionQuestion = null;
	[SerializeField] private DescriptionField descriptionAnswer1 = null;
	[SerializeField] private DescriptionField descriptionAnswer2 = null;
	[SerializeField] private DescriptionField descriptionAnswer3 = null;
	[SerializeField] private DescriptionField descriptionScratchWonMessage = null;
	[SerializeField] private DescriptionField descriptionScratchLostMessage = null;
	[SerializeField] private DescriptionField descriptionScratchLink = null;
	[SerializeField] private SelectImage selectImageGoods = null;
	[SerializeField] private SelectImage selectImageGoodsBackground = null;
	[SerializeField] private SelectImage selectImageSale = null;
	[SerializeField] private SelectImage selectImageQuizesBackground = null;
	[SerializeField] private SelectImage selectImageQuizesLogo = null;
	[SerializeField] private SelectImage selectImageScratchBackground = null;
	[SerializeField] private SelectImage selectImageScratchLogo = null;
	[SerializeField] private SelectColor selectColorQuestion = null;
	[SerializeField] private SelectColor selectColorAnswer1 = null;
	[SerializeField] private SelectColor selectColorAnswer2 = null;
	[SerializeField] private SelectColor selectColorAnswer3 = null;
	[SerializeField] private SelectColor selectColorScratchWon = null;
	[SerializeField] private SelectColor selectColorScratchLost = null;
	[SerializeField] private PriceButtons priceButtons = null;
	[SerializeField] private PriceButtons rightAnswerButtons = null;
	[HideInInspector] public AdvertisementType advertisementType = AdvertisementType.Promotion;
	[HideInInspector]public int AdvIndex = 0;
	private string path = "";
	
	
	public void ParseData( AdvertisementData data)
	{
		AdvData = data;
		if (data.Promotions.Count > 0)
			UpdatePanelConfiguration(AdvData, AdvertisementType.Promotion, 0);
		if (data.Quizes.Count > 0)
			UpdatePanelConfiguration(AdvData, AdvertisementType.Quiz, 0);
		UpdatePanelConfiguration(AdvData, AdvertisementType.Scratch, 0);
	}

		public void UpdatePanelConfiguration(AdvertisementData data, AdvertisementType advertisementType, int index )
		{
			sliderPromotionChance.value = data.PromotionChance;	
			textPromotionChance.text = data.PromotionChance.ToString();
			sliderQuizChance.value = data.QuizChance;	
			textQuizChance.text = data.QuizChance.ToString();
			sliderScratchChance.value = data.ScratchChance;	
			textScratchChance.text = data.ScratchChance.ToString();

			numberPromotion.NumberAll = data.Promotions.Count;
			numberQuizes.NumberAll = data.Quizes.Count;
			switch (advertisementType)
			{
			case AdvertisementType.Promotion:
			{
				for(int i = 0; i < data.Promotions.Count; i++)
				{							
					sliderSaleAmount.value = AdvData.Promotions[index].SaleAmount;
					textSaleAmount.text = AdvData.Promotions[index].SaleAmount.ToString();
					
					sliderSaleRotation.value = AdvData.Promotions[index].SaleRotation;
					textSaleRotation.text = AdvData.Promotions[index].SaleRotation.ToString();
					
					sliderSkipTime.value = AdvData.Promotions[index].SkipTime;
					textSkipTime.text = AdvData.Promotions[index].SkipTime.ToString();
					
					if (data.Promotions[index].GoodsLayout == PromotionData.PromotionLayout.Middle)
					{layoutButtonsGoods.Middle();}
					if (data.Promotions[index].GoodsLayout == PromotionData.PromotionLayout.MiddleLeft)
					{layoutButtonsGoods.MiddleLeft();}
					if (data.Promotions[index].GoodsLayout == PromotionData.PromotionLayout.MiddleRight)
					{layoutButtonsGoods.MiddleRight();}
					if (data.Promotions[index].GoodsLayout == PromotionData.PromotionLayout.BottomMiddle)
					{layoutButtonsGoods.ButtomMiddle();}
					if (data.Promotions[index].GoodsLayout == PromotionData.PromotionLayout.BottomLeft)
					{layoutButtonsGoods.ButtomLeft();}
					if (data.Promotions[index].GoodsLayout == PromotionData.PromotionLayout.BottomRight)
					{layoutButtonsGoods.ButtomRight();}
					if (data.Promotions[index].GoodsLayout == PromotionData.PromotionLayout.TopMiddle)
					{layoutButtonsGoods.TopMiddle();}
					if (data.Promotions[index].GoodsLayout == PromotionData.PromotionLayout.TopLeft)
					{layoutButtonsGoods.TopLeft();}
					if (data.Promotions[index].GoodsLayout == PromotionData.PromotionLayout.TopRight)
					{layoutButtonsGoods.TopRight();}
					
					if (data.Promotions[index].SaleLayout == PromotionData.PromotionLayout.Middle)
					{layoutButtonsSale.Middle();}
					if (data.Promotions[index].SaleLayout == PromotionData.PromotionLayout.MiddleLeft)
					{layoutButtonsSale.MiddleLeft();}
					if (data.Promotions[index].SaleLayout == PromotionData.PromotionLayout.MiddleRight)
					{layoutButtonsSale.MiddleRight();}
					if (data.Promotions[index].SaleLayout == PromotionData.PromotionLayout.BottomMiddle)
					{layoutButtonsSale.ButtomMiddle();}
					if (data.Promotions[index].SaleLayout == PromotionData.PromotionLayout.BottomLeft)
					{layoutButtonsSale.ButtomLeft();}
					if (data.Promotions[index].SaleLayout == PromotionData.PromotionLayout.BottomRight)
					{layoutButtonsSale.ButtomRight();}
					if (data.Promotions[index].SaleLayout == PromotionData.PromotionLayout.TopMiddle)
					{layoutButtonsSale.TopMiddle();}
					if (data.Promotions[index].SaleLayout == PromotionData.PromotionLayout.TopLeft)
					{layoutButtonsSale.TopLeft();}
					if (data.Promotions[index].SaleLayout == PromotionData.PromotionLayout.TopRight)
					{layoutButtonsSale.TopRight();}
					
					if (data.Promotions[index].TextLayout == PromotionData.PromotionLayout.Middle)
					{layoutButtonsText.Middle();}
					if (data.Promotions[index].TextLayout == PromotionData.PromotionLayout.MiddleLeft)
					{layoutButtonsText.MiddleLeft();}
					if (data.Promotions[index].TextLayout == PromotionData.PromotionLayout.MiddleRight)
					{layoutButtonsText.MiddleRight();}
					if (data.Promotions[index].TextLayout == PromotionData.PromotionLayout.BottomMiddle)
					{layoutButtonsText.ButtomMiddle();}
					if (data.Promotions[index].TextLayout == PromotionData.PromotionLayout.BottomLeft)
					{layoutButtonsText.ButtomLeft();}
					if (data.Promotions[index].TextLayout == PromotionData.PromotionLayout.BottomRight)
					{layoutButtonsText.ButtomRight();}
					if (data.Promotions[index].TextLayout == PromotionData.PromotionLayout.TopMiddle)
					{layoutButtonsText.TopMiddle();}
					if (data.Promotions[index].TextLayout == PromotionData.PromotionLayout.TopLeft)
					{layoutButtonsText.TopLeft();}
					if (data.Promotions[index].TextLayout == PromotionData.PromotionLayout.TopRight)
					{layoutButtonsText.TopRight();}
					
					descriptionField.inputField.text = data.Promotions[index].Description;
					descriptionFieldLink.inputField.text = data.Promotions[index].HttpLink;
					descriptionPrice.inputField.text = data.Promotions[index].Price.ToString();
					selectImageGoods.buttonText.text = data.Promotions[index].GoodsImageName;
					selectImageSale.buttonText.text = data.Promotions[index].SaleImageName;
					selectImageGoodsBackground.buttonText.text = data.Promotions[index].BackgroundImageName;
					StartCoroutine(UploadPNG(selectImageGoodsBackground.rawIage, selectImageGoodsBackground.saveImagePath + data.Promotions[index].BackgroundImageName));
					StartCoroutine(UploadPNG(selectImageSale.rawIage, selectImageSale.saveImagePath + data.Promotions[index].SaleImageName));
					StartCoroutine(UploadPNG(selectImageGoods.rawIage, selectImageGoods.saveImagePath + data.Promotions[index].GoodsImageName));

					if (data.Promotions[index].Currency == PromotionData.CurrencyType.Dollar)
						priceButtons.SetDoll();
					if (data.Promotions[index].Currency == PromotionData.CurrencyType.Euro)
						priceButtons.SetEuro();
					if (data.Promotions[index].Currency == PromotionData.CurrencyType.Pound)
						priceButtons.SetPound();
				}
				break;
			}
			case AdvertisementType.Quiz:
			{
				for(int i = 0; i < data.Quizes.Count; i++)				
				{
					selectImageQuizesBackground.buttonText.text = data.Quizes[index].BackgroundImageName;
					selectImageQuizesLogo.buttonText.text = data.Quizes[index].LogoImageName;
					StartCoroutine(UploadPNG(selectImageQuizesLogo.rawIage, selectImageQuizesLogo.saveImagePath + data.Quizes[index].LogoImageName));
					StartCoroutine(UploadPNG(selectImageQuizesBackground.rawIage, selectImageQuizesBackground.saveImagePath + data.Quizes[index].BackgroundImageName));
					selectColorQuestion.buttonImage.color = data.Quizes[index].Question.QuestionTextColor;
					selectColorAnswer1.buttonImage.color = data.Quizes[index].Question.Answer1Color;
					selectColorAnswer2.buttonImage.color = data.Quizes[index].Question.Answer2Color;
					selectColorAnswer3.buttonImage.color = data.Quizes[index].Question.Answer3Color;
					selectColorQuestion.GettingColor = data.Quizes[index].Question.QuestionTextColor;
					selectColorAnswer1.GettingColor = data.Quizes[index].Question.Answer1Color;
					selectColorAnswer2.GettingColor = data.Quizes[index].Question.Answer2Color;
					selectColorAnswer3.GettingColor = data.Quizes[index].Question.Answer3Color;
					descriptionQuestion.inputField.text = data.Quizes[index].Question.QuestionText;
					descriptionAnswer1.inputField.text = data.Quizes[index].Question.Answer1;
					descriptionAnswer2.inputField.text = data.Quizes[index].Question.Answer2;
					descriptionAnswer3.inputField.text = data.Quizes[index].Question.Answer3;
					if (data.Quizes[index].Question.CorrectAnswer == 0)
						rightAnswerButtons.SetDoll();
					if (data.Quizes[index].Question.CorrectAnswer == 1)
						rightAnswerButtons.SetEuro();
					if (data.Quizes[index].Question.CorrectAnswer == 2)
						rightAnswerButtons.SetPound();
				}
				break;
			}
			case AdvertisementType.Scratch:
			{
				selectImageScratchBackground.buttonText.text = data.ScratchData.BackgroundImageName;
				selectImageScratchLogo.buttonText.text = data.ScratchData.LogoImageName;
				StartCoroutine(UploadPNG(selectImageScratchLogo.rawIage, selectImageScratchLogo.saveImagePath + data.ScratchData.LogoImageName));
				StartCoroutine(UploadPNG(selectImageScratchBackground.rawIage, selectImageScratchBackground.saveImagePath + data.ScratchData.BackgroundImageName));

				descriptionScratchWonMessage.inputField.text = data.ScratchData.WonMessage;
				descriptionScratchLostMessage.inputField.text = data.ScratchData.LostMessage;
				descriptionScratchLink.inputField.text = data.ScratchData.WinUrl;
				selectColorScratchWon.buttonImage.color = data.ScratchData.WonMessageColor;
				selectColorScratchWon.GettingColor = data.ScratchData.WonMessageColor;
				selectColorScratchLost.buttonImage.color = data.ScratchData.LostMessageColor;
				selectColorScratchLost.GettingColor = data.ScratchData.LostMessageColor;
				break;
			}
			}
		}

	public void GetCurrentAdvertisement( ref AdvertisementType type, ref int index )
	{
		type = advertisementType;
		index = AdvIndex;	
	}

	public IEnumerator UploadPNG(RawImage rImage, string fName)
	{
		yield return new WaitForEndOfFrame();
		path = Application.dataPath + "/" + fName + ".png";
		rImage.texture = null;
			//loadingText.enabled = true;
			//saveButton.gameObject.SetActive(false);
		if (File.Exists(path))
		{
			WWW www = new WWW("file://" + path);
			yield return www;
			//loadingText.enabled = false;
			//saveButton.gameObject.SetActive(true);
			Texture2D tex2d = www.texture;
			rImage.texture = tex2d;
		}
	}
}
}