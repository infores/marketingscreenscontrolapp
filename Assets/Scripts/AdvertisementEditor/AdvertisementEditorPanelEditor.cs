﻿//
// AdvertisementEditorPanelEditor.cs
//
// Created by Lazarevich Aleksey on 28.06.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//


using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Geec.Utils;


namespace Geec.Advertisement.EditorApplication
{
public class AdvertisementEditorPanelEditor : MonoBehaviour 
{
	public AdvertisementEditorView View;
	public GameObject PanelTest;
	public AdvertisementEditorPanelConfiguration PanelConfiguration;
	public GameObject PanelUpload;
	public AdvertisementEditorPanelNotification PanelNotification;
	public GameObject PanelUploading;
	
	
	void Awake()
	{
		PanelNotification.gameObject.SetActive( true );
	}
	
	public void ParseData( AdvertisementData data )
	{
		if( data == null )
		{
			return;
		}
		
		PanelConfiguration.ParseData( data );
	}
	
	public void OnTestClick()
	{
		View.UpdateData( PanelConfiguration.AdvData );
		AdvertisementType type = AdvertisementType.Promotion;
		int index = 0;
		
		PanelConfiguration.GetCurrentAdvertisement( ref type, ref index );
		View.Test( type, index );
	}
	
	public void OnLoaded()
	{
		PanelNotification.gameObject.SetActive( false );
	}
	
	public void OnLoadingErrors( List<string> errors = null )
	{
		PanelNotification.SetErrors( errors );
	}
	
	public void OnSaveClick()
	{
		View.SaveConfig();		
	}
	
	public void OnUploadClick()
	{
		PanelTest.SetActive( false );
		PanelConfiguration.gameObject.SetActive( false );
		PanelUpload.SetActive( false );
		PanelUploading.SetActive( true );
	}
	
	public void OnPanelUploadingClosed()
	{
		PanelTest.SetActive( true );
		PanelConfiguration.gameObject.SetActive( true );
		PanelUpload.SetActive( true );
		PanelUploading.SetActive( false );
	}
}
}