﻿//
// DescriptionField.cs
//
// Created by Berezin Boris on 13.07.2015.
// Copyright (c) 2015 Berezin Boris. All rights reserved.
//
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Geec.Advertisement.EditorApplication;

namespace Geec.Advertisement.EditorApplication
{
	internal enum DescriptionType
	{
		PromotionDescription,
		PromotionLink,
		Price,
		QuizesQuestion,
		QuizesAnswer1,
		QuizesAnswer2,
		QuizesAnswer3,
		ScratchWonMessage,
		ScratchLostMessage,
		ScratchWonLink
	}

	public class DescriptionField : MonoBehaviour 
	{
		[SerializeField] private AdvertisementEditorPanelConfiguration advertisementEditorPanelConfiguration = null;
		[SerializeField] private DescriptionType descriptionType = DescriptionType.PromotionDescription;
		[SerializeField] private Number number = null;
		//public Text descriptionText = null;
		public InputField inputField = null;

		public void EndEdit()
		{
			switch (descriptionType)
			{
			case DescriptionType.PromotionDescription:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].Description = inputField.text; 
				break;
			}
			case DescriptionType.PromotionLink:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].HttpLink = inputField.text; 
				break;
			}
			case DescriptionType.Price:
			{
				try
				{
					advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].Price = Int32.Parse(inputField.text);
				}
				catch 
				{
					Debug.LogWarning("");
				}
				break;
			}
			case DescriptionType.QuizesQuestion:
			{
				advertisementEditorPanelConfiguration.AdvData.Quizes[number.CurrentNumber].Question.QuestionText = inputField.text; 
				break;
			}
			case DescriptionType.QuizesAnswer1:
			{
				advertisementEditorPanelConfiguration.AdvData.Quizes[number.CurrentNumber].Question.Answer1 = inputField.text; 
				break;
			}
			case DescriptionType.QuizesAnswer2:
			{
				advertisementEditorPanelConfiguration.AdvData.Quizes[number.CurrentNumber].Question.Answer2 = inputField.text; 
				break;
			}
			case DescriptionType.QuizesAnswer3:
			{
				advertisementEditorPanelConfiguration.AdvData.Quizes[number.CurrentNumber].Question.Answer3 = inputField.text; 
				break;
			}
			case DescriptionType.ScratchWonMessage:
			{
				advertisementEditorPanelConfiguration.AdvData.ScratchData.WonMessage = inputField.text; 
				break;
			}
			case DescriptionType.ScratchLostMessage:
			{
				advertisementEditorPanelConfiguration.AdvData.ScratchData.LostMessage = inputField.text; 
				break;
			}
			case DescriptionType.ScratchWonLink:
			{
				advertisementEditorPanelConfiguration.AdvData.ScratchData.WinUrl = inputField.text; 
				break;
			}
			}
		}
	}
}
