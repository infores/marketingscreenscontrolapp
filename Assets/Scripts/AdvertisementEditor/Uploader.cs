﻿//
// Uploader.cs
//
// Created by Berezin Boris on 13.07.2015.
// Copyright (c) 2015 Berezin Boris. All rights reserved.
//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.IO;
using System.Collections.Generic;
using Geec.Advertisement;
using Geec.Utils;

namespace Geec.Advertisement.EditorApplication
{
	public class Uploader : MonoBehaviour 
	{
		public AdvertisementEditorPanelEditor PanelEditor;
		private string uploadPath = "http://lasergame.servegame.com/GeecAds/";
		[SerializeField] private string CompanyName = "CoolCompanyName";
		const string configFilePath = "Configuration/config.xml";
		[SerializeField] private Text textStatus = null;
		[SerializeField] private Text textResult = null;
		[SerializeField] private Button buttonClose = null;


		void OnEnable()
		{
			textStatus.text = "Waiting for answer from server...";
			textStatus.color = Color.black;

			textResult.color = Color.black;
			textResult.gameObject.SetActive( false );
			
			buttonClose.gameObject.SetActive( false );
			
			StartCoroutine( UploadConfig() );
		}

		private Dictionary<string, Dictionary<string, string>> SelectFilesInFolders( out string errorMessage )
		{
			errorMessage = null;
			Dictionary<string, Dictionary<string, string>> result = new Dictionary<string, Dictionary<string, string>>();
			
			List<string> folders = FileSelector.GetChildDirectories( Application.dataPath + @"/Configuration/Sprites/" );
						
			foreach( string folder in folders )
			{			
				Dictionary<string, string> filesNamesPaths = new Dictionary<string, string>();
			
				List<string> names = FileSelector.GetFiles(Application.dataPath + @"/Configuration/Sprites/" + folder + @"/", false, ".png");
				List<string> paths = FileSelector.GetFiles(Application.dataPath + @"/Configuration/Sprites/" + folder + @"/", true, ".png");
				
				if( names.Count == paths.Count )
				{
					for( int i=0; i<names.Count; i++ )
					{
						try
						{
							filesNamesPaths.Add( names[i], paths[i] );
						}
						catch( Exception ex )
						{
							Debug.LogWarning( "Uploader::SelectFilesInFolder(): " + ex.Message );
							errorMessage = "Duplicate " + folder + " images.";
							return null;
						}
					}
					
					try
					{
						result.Add( folder, filesNamesPaths );
					}
					catch( Exception ex )
					{
						Debug.LogWarning( "Uploader::SelectFilesInFolder(): " + ex.Message );
						errorMessage = "Duplicate " + folder + " folders.";
						return null;
					}
				}
				else
				{
					Debug.LogError( "Uploade::SelectFilesInFolder(): loading controlls images error." );
					errorMessage = "Error during opening " + folder + " images.";
					return null;
				}
			}
			
			return result;
		}
		
		IEnumerator UploadPicture()
		{
			yield return new WaitForSeconds( 1 );
			
			string errorMessage = null;
			Dictionary<string, Dictionary<string, string>> foldersAndFiles = SelectFilesInFolders( out errorMessage );
			
			if( foldersAndFiles == null )
			{
				errorMessage = "Error opening images files.";
			}
			else if( errorMessage == null )
			{
				foreach( KeyValuePair<string, Dictionary<string, string>> kvpFolder in foldersAndFiles )
				{
					if( errorMessage != null )
					{
						break;
					}
				
					foreach( KeyValuePair<string, string> kvpFilesNamesPaths in kvpFolder.Value )
					{
						WWW localFile = new WWW("file://" + kvpFilesNamesPaths.Value);
						yield return localFile;
						
						WWWForm postForm = new WWWForm();
						postForm.AddBinaryData( kvpFolder.Key, localFile.bytes, kvpFilesNamesPaths.Value, "image/png" );
						WWW upload = new WWW(uploadPath + CompanyName + "/image.php", postForm);
						yield return upload;
						
						if (upload.error == null)
						{
							textStatus.text = upload.text;
							//Debug.Log(upload.text);
						}
						else
						{
							errorMessage = "Error during upload " + kvpFilesNamesPaths.Key;
							break;
						}
					}
				}
			}
			
			if( errorMessage != null )
			{
				textStatus.text = errorMessage;
				textStatus.color = Color.red;
				textResult.color = Color.red;
				
				textResult.text = "Failed";
				
				Debug.LogError( errorMessage );
			}
			else
			{
				textStatus.text = "Configuration file and images uploaded.";
				textResult.text = "Succeded";
			}
			
			textResult.gameObject.SetActive(true);
			buttonClose.gameObject.SetActive(true);
		}

		IEnumerator UploadConfig()
		{
			yield return new WaitForSeconds( 2 );
			
			WWW localFile = new WWW("file://" + Application.dataPath + @"/Configuration/config.xml");
			yield return localFile;
			
			WWWForm postForm = new WWWForm();
			postForm.AddBinaryData("Config", localFile.bytes, "config.xml", "application/octet-stream");
			WWW upload = new WWW(uploadPath + CompanyName + "/ClearDirectories.php", postForm);
			yield return upload;
			
			
			if( upload.error != null )
			{
				Debug.LogError( "Connection error." );
				
				textStatus.gameObject.SetActive(true);
				textStatus.color = Color.red;
				textStatus.text = "Connection error.";
				textResult.text = "Failed";
				textResult.color = Color.red;
				textResult.gameObject.SetActive( true );
				buttonClose.gameObject.SetActive(true);
			}
			else if( upload.text != "Start Uploading" )
			{
				Debug.LogError("Error during upload config.");
				
				textStatus.gameObject.SetActive(true);
				textStatus.color = Color.red;
				textStatus.text = "Error during upload config.";
				textResult.text = "Failed";
				textResult.color = Color.red;
				textResult.gameObject.SetActive( true );
				buttonClose.gameObject.SetActive(true);
			}
			else
			{
				textStatus.gameObject.SetActive(true);
				
				textStatus.text = upload.text;
				
				yield return new WaitForSeconds( 1 );
				
				textStatus.text = "Config.xml uploaded.";
				
				StartCoroutine(UploadPicture());
			}
		}

		public void Close()
		{
			gameObject.SetActive( false );
			PanelEditor.OnPanelUploadingClosed();
		}
	}
}
