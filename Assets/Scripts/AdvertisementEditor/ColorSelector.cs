﻿//
// ColorSelector.cs
//
// Created by Berezin Boris on 13.07.2015.
// Copyright (c) 2015 Berezin Boris. All rights reserved.
//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace Geec.Advertisement.EditorApplication
{
	public class ColorSelector : MonoBehaviour 
	{
		[SerializeField] private Slider rSlider = null;
		[SerializeField] private Slider gSlider = null;
		[SerializeField] private Slider bSlider = null;
		[SerializeField] private Slider aSlider = null;
		[SerializeField] private Image colorField = null;
		[SerializeField] private Text rText = null;
		[SerializeField] private Text gText = null;
		[SerializeField] private Text bText = null;
		[SerializeField] private Text aText = null;
		public SelectColor selectColor = null;

		public void Close () 
		{
			selectColor.SetColor();
			Destroy (gameObject);
		}
		
		private void Update () 
		{
			colorField.color = new Color(rSlider.value, gSlider.value, bSlider.value, aSlider.value);
			rText.text = (rSlider.value * 255).ToString("f0");
			gText.text = (gSlider.value * 255).ToString("f0");
			bText.text = (bSlider.value * 255).ToString("f0");
			aText.text = (aSlider.value * 255).ToString("f0");
			selectColor.GettingColor = new Color(rSlider.value, gSlider.value, bSlider.value, aSlider.value);
			selectColor.buttonImage.color = selectColor.GettingColor;
		}

		public void SetColor(Color newColor)
		{
			rSlider.value = newColor.r;
			gSlider.value = newColor.g;
			bSlider.value = newColor.b;
			aSlider.value = newColor.a;
		}
	}
}
