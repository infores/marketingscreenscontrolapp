﻿//
// AdvertisementEditorImagesLoader.cs
//
// Created by Lazarevich Aleksey on 28.06.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Geec.Advertisement;
using System;

namespace Geec.Advertisement.EditorApplication
{
public class AdvertisementEditorImagesLoader : MonoBehaviour 
{
	public AdvertisementEditorController Controller;

	public void LoadImages()
	{
		SelectSpriteFolder(currentSpriteFolder);				
		StartCoroutine(UploadPicture());		
	}

	[Serializable] public class FolderPropetries
	{
		public string name = "Backgrounds";
		public ImageType imageType = ImageType.Background;
		public float width = 0;
		public float height = 0;
	}	
	
	[SerializeField] private FolderPropetries[] folderPropetries = null;	
	Dictionary<ImageType, List<Texture2D>> result = new Dictionary<ImageType, List<Texture2D>>();
	private List<Texture2D> tempTextures =  new List<Texture2D>();
	List<string> errors = new List<string>();
	private List<string> files;
	private List<string> fileNames;
	private int loadedFiles = 0;
	private int currentFile = 0;
	private int currentSpriteFolder = 0;
	private bool isPng = true;
	byte[] png_signature = {137, 80, 78, 71, 13, 10, 26, 10};	

	private void SelectSpriteFolder(int spriteFolder)
	{
		if (folderPropetries.Length > 0)
		{
			files = FileSelector.GetFiles(Application.dataPath + @"/Configuration/Sprites/" + folderPropetries[currentSpriteFolder].name + @"/", true, ".png");
			fileNames = FileSelector.GetFiles(Application.dataPath + @"/Configuration/Sprites/" + folderPropetries[currentSpriteFolder].name + @"/", false, ".png");		
			for (int i = 0; i < fileNames.Count; i++)
			{
				fileNames[i] = fileNames[i].Substring(0, fileNames[i].Length - 4);
			}
		}
	}

	IEnumerator UploadPicture()
	{
		yield return new WaitForEndOfFrame();
		if (files.Count > 0)
		{
			WWW www = new WWW("file://" + files[currentFile]);
			yield return www;
			for (int i = 0; i < png_signature.Length; i++)
			{
				if (www.bytes[i] != png_signature[i])
				{
					isPng = false;
					string texName = folderPropetries[currentSpriteFolder].name + "/" + fileNames[currentFile].Substring(0,fileNames[currentFile].Length - 4);
						errors.Add ("Texture: " + texName + "is incorrect .Png file");
				}
			}
			if (isPng)
			{
				Texture2D tex2d = www.texture;
				tex2d.name = fileNames[currentFile];
				if (tex2d.width != folderPropetries[currentSpriteFolder].width || 
				    tex2d.height != folderPropetries[currentSpriteFolder].height)
				{
					string texName = folderPropetries[currentSpriteFolder].name + "/" + fileNames[currentFile];
					errors.Add ("Texture: " + texName + " size must be " + folderPropetries[currentSpriteFolder].width + " x " 
					            + folderPropetries[currentSpriteFolder].width);
				}
				tempTextures.Add(tex2d);
			}
		}
		currentFile ++;
		if (currentFile < files.Count)
			StartCoroutine(UploadPicture());
		else
		{
			result.Add(folderPropetries[currentSpriteFolder].imageType, new List<Texture2D>(tempTextures));
			tempTextures.Clear ();
			currentSpriteFolder++;
			loadedFiles += files.Count;
			currentFile = 0;
			if (folderPropetries.Length > currentSpriteFolder)
			{
				SelectSpriteFolder(currentSpriteFolder);
				StartCoroutine(UploadPicture());
			}
			else
			{
				Controller.ImagesLoaded(result, errors);
			}
		}			
	}
	
}
}