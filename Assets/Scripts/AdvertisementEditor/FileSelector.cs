//
// FileSelector.cs
//
// Created by Berezin Boris on 13.07.2015.
// Copyright (c) 2015 Berezin Boris. All rights reserved.
//
using System;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace Geec.Advertisement.EditorApplication
{
	public class FileSelector : MonoBehaviour 
	{
		public string extension = ".*";
		public string saveImagePath = "Sprites/Backgrounds/";
		[SerializeField] private RawImage rawImage = null;		
		public string path = "";
		private string file = "";
		private List<string> parentDirs = new List<string>();
		//private List<string> childDirs = new List<string>();
		private List<string> pictures = new List<string>();
		//private List<string> drivs = new List<string>();
		//private GameObject[] buttonsDriveObjs = new GameObject[0];
		private List<GameObject> buttonsGoParentFolderObjs = new List<GameObject>();
		//private List<GameObject> buttonsGoChildFolderObjs = new List<GameObject>();
		private List<GameObject> buttonsFilesObjs = new List<GameObject>();
		[SerializeField] private GameObject buttonGoParentFolderPrefab;
		[SerializeField] private GameObject buttonGoChildFolderPrefab;
		[SerializeField] private GameObject buttonFilePrefab;
		[SerializeField] private GameObject buttonDrivePrefab;
		[SerializeField] private Text loadingText = null;
		[SerializeField] private Button saveButton = null;
		[SerializeField] private Transform parentButtonsPath = null;
		[SerializeField] private Transform parentButtonsImages = null;
		private byte[] bytes = null;
		[HideInInspector] public SelectImage selectImage = null;

		private void OnEnable()
		{
			UpdateWindow(false);
			loadingText.enabled = false;
			saveButton.gameObject.SetActive(false);
		}

		private void UpdateWindow(bool showCurrentDirectory = true)
		{
			parentDirs = GetParentDirectories(path);
			//childDirs = GetChildDirectories(path);
			pictures = GetFiles(path, extension : extension);
			//drivs = new List<string>( System.IO.Directory.GetLogicalDrives()) ;

//			foreach(var button in buttonsDriveObjs)
//			{
//				Destroy (button);
//			}
//			border = (actualAspect - 1.33f) * 278 * scaleFactor + scaleFactor * 80;
//			Array.Resize(ref buttonsDriveObjs, drivs.Length);
//			for(int i = 0; i < drivs.Length; i++)
//			{
//				buttonsDriveObjs[i] = Instantiate (buttonDrivePrefab, Vector3.zero, Quaternion.identity) as GameObject;
//				buttonsDriveObjs[i].transform.position = new Vector3(i * 60 * scaleFactor + border, 510*scaleFactor, 0);
//				buttonsDriveObjs[i].transform.localScale = Vector3.one * scaleFactor;
//				buttonsDriveObjs[i].transform.SetParent(generatedButtons);
//				buttonsDriveObjs[i].GetComponentInChildren<Text>().text = drivs[i];
//				int num = i;
//				buttonsDriveObjs[i].GetComponent<Button>().onClick.AddListener(() =>  GoDrive(num));
//			}

			buttonsGoParentFolderObjs.Clear();

			for(int i = 0; i < parentDirs.Count; i++)
			{
				CreateParentButton(i, parentDirs[i]);
			}
			
//			if (showCurrentDirectory)
//			{
//				string currentDirectory = new FileInfo(path).Name;
//				if (currentDirectory == "")
//				{
//					currentDirectory = path;
//				}
//				
//				CreateParentButton( parentDirs.Count, currentDirectory );
//			}

//			buttonsGoChildFolderObjs.Clear();
//
//			int horNumber = 0;
//			int vertNuber = 0;
//			float vertStepAmongButtons = 40 * scaleFactor;
//			for(int i = 0; i < childDirs.Count; i++)
//			{
//				GameObject button = Instantiate (buttonGoChildFolderPrefab, Vector3.zero, Quaternion.identity) as GameObject;
//				button.transform.position = new Vector3(horNumber*100 * scaleFactor + border, 450*scaleFactor - vertNuber*vertStepAmongButtons, 0);
//				button.transform.localScale = Vector3.one * scaleFactor;
//				button.transform.SetParent(generatedButtons);
//				button.GetComponentInChildren<Text>().text = childDirs[i];
//				horNumber++;
//				if (horNumber > 6)
//				{
//					horNumber = 0;
//					vertNuber ++;
//				}
//				int num = i;
//				button.GetComponent<Button>().onClick.AddListener(() =>  GoChildFolder(num));
//				
//				buttonsGoChildFolderObjs.Add( button );
//			}


			buttonsFilesObjs.Clear();

			for( int i = 0; i < pictures.Count; i++ )
			{
				GameObject button = Instantiate( buttonFilePrefab, Vector3.zero, Quaternion.identity ) as GameObject;
				button.transform.SetParent( parentButtonsImages, false );
				button.GetComponentInChildren<Text>().text = pictures[i];

				int num = i;
				button.GetComponent<Button>().onClick.AddListener(() =>  SelectPicture(num));
				
				buttonsFilesObjs.Add( button );
			}
		}

		private void CreateParentButton(int i, string text)
		{
			GameObject button = Instantiate( buttonGoParentFolderPrefab, Vector3.zero, Quaternion.identity ) as GameObject;
			button.transform.SetParent(parentButtonsPath, false);
			button.GetComponentInChildren<Text>().text = text;
			
			buttonsGoParentFolderObjs.Add( button );
		}

		public void GoParentFolder(int num)
		{
			//if (num < parentDirs.Length)		
			//	path = GetParentDirectories(path, true)[num];
			//UpdateWindow();
		}

		public void GoChildFolder(int num)
		{
			//path = GetChildDirectories(path, true)[num];
			//UpdateWindow();
		}

		public void GoDrive(int num)
		{
			//path = drivs[num];
			//UpdateWindow();
		}

		public void SelectPicture(int num)
		{
			file = pictures[num];
			StartCoroutine(UploadPNG(rawImage));
		}

		public void Close()
		{
			StopAllCoroutines();
			
			var children = new List<GameObject>();
			foreach( Transform child in parentButtonsPath.transform )
			{ 
				children.Add(child.gameObject);
			}
			foreach( Transform child in parentButtonsImages.transform )
			{ 
				children.Add(child.gameObject);
			}
			children.ForEach( child => Destroy( child ) );
			
			rawImage.texture = null;
			
			gameObject.SetActive( false );
		}	
			
		private static List<string> GetParentDirectories(string filePath, bool includePaths = false)
		{
			List<string> parents = new List<string>();
			FileInfo fileInfo;

			while(true){
				try {
					fileInfo = new FileInfo(filePath);
					if(!includePaths) parents.Add(fileInfo.Directory.Name);
					else parents.Add(fileInfo.Directory.FullName);
					
					filePath = fileInfo.Directory.FullName;
				}
				
				catch {break;}
			}
			
			parents.Reverse();
			return parents;		
		}
		
		public static List<string> GetChildDirectories(string directoryPath, bool includePaths = false)
		{
			DirectoryInfo directory;
			
			if( Directory.Exists(directoryPath) )
			{
				directory = new DirectoryInfo(directoryPath);
			}
			else
			{
				try{ directory = new FileInfo(directoryPath).Directory;	}
				catch{ return new List<string>(); }
			}
			
			List<string> children = new List<string>();
			
			try{
				DirectoryInfo[] directories = directory.GetDirectories();
				
				foreach(DirectoryInfo childDir in directories){
					if(!includePaths) children.Add(childDir.Name);
					else children.Add(childDir.FullName);
				}			
			}
			
			catch
			{
				children = new List<string>();
			}
			
			return children;
		}
		
		public static List<string> GetFiles(string directoryPath, bool includePaths = false, string extension = ".*")
		{
			DirectoryInfo directory;
			
			if(Directory.Exists(directoryPath)) directory = new DirectoryInfo(directoryPath);
			else
			{
				try{ directory = new FileInfo(directoryPath).Directory;	}
				catch{ return new List<string>(); }
			}
			
			List<string> files = new List<string>();
			
			try{
				FileInfo[] fileInfos = directory.GetFiles("*"+extension);
				
				foreach(FileInfo fileInfo in fileInfos){
					if(!includePaths) files.Add(fileInfo.Name);	
					else files.Add(fileInfo.FullName);	
				}
			}
			
			catch{
				files = new List<string>();
			}
			
			return files;
		}	

		IEnumerator UploadPNG(RawImage rImage)
		{
			yield return new WaitForEndOfFrame();
			rImage.texture = null;
			loadingText.enabled = true;
			saveButton.gameObject.SetActive(false);
			WWW www = new WWW("file://" + path+@"/"+file);
			yield return www;
			loadingText.enabled = false;
			saveButton.gameObject.SetActive(true);
			Texture2D tex2d = www.texture;
			rImage.texture = tex2d;
			var tex = www.texture;
			bytes = tex.EncodeToPNG();
			Destroy(tex);
		}

		public void SavePicture()
		{
			if (bytes != null)
			{
				//Save file
				//string savePath = Application.dataPath + @"\"+ saveImagePath + file;		
				//FileStream fs = File.Create(savePath);
				//fs.Write(bytes, 0, bytes.Length);
				//fs.Close();

				string picName = file.Substring(0, file.Length - 4);
				selectImage.SetImageName(picName);
			}
			Close();
		}
	}
}
