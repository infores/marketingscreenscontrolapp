﻿//
// SelectImage.cs
//
// Created by Berezin Boris on 13.07.2015.
// Copyright (c) 2015 Berezin Boris. All rights reserved.
//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Geec.Advertisement.EditorApplication;
namespace Geec.Advertisement.EditorApplication
{
	internal enum ImageName
	{
		PromotionGoods,
		PromotionBackground,
		PromotionSale,
		QuizesLogo,
		QuizesBackground,
		ScratchLogo,
		ScratchBackground
	}

	public class SelectImage : MonoBehaviour 
	{
		[SerializeField] private AdvertisementEditorPanelConfiguration advertisementEditorPanelConfiguration = null;
		[SerializeField] private FileSelector fileSelector = null;
		[SerializeField] private ImageName imageName = ImageName.PromotionGoods;
		[SerializeField] private Number number = null;
		public RawImage rawIage = null;
		public string saveImagePath = "Sprites/Backgrounds/";
		public Text buttonText = null;
			
		public void OpenImage() 
		{
			fileSelector.saveImagePath = saveImagePath;
			fileSelector.selectImage = this;
			fileSelector.path = Application.dataPath + @"/" + saveImagePath ;
			fileSelector.gameObject.SetActive( true );
		}

		public void SetImageName(string picName)
		{
			buttonText.text = picName;
			switch (imageName)
			{
			case ImageName.PromotionBackground:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].BackgroundImageName = picName;
				advertisementEditorPanelConfiguration.UpdatePanelConfiguration(advertisementEditorPanelConfiguration.AdvData, advertisementEditorPanelConfiguration.advertisementType, advertisementEditorPanelConfiguration.AdvIndex);
				break;
			}
			case ImageName.PromotionGoods:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].GoodsImageName = picName;
				advertisementEditorPanelConfiguration.UpdatePanelConfiguration(advertisementEditorPanelConfiguration.AdvData, advertisementEditorPanelConfiguration.advertisementType, advertisementEditorPanelConfiguration.AdvIndex);

				break;
			}
			case ImageName.PromotionSale:
			{
				advertisementEditorPanelConfiguration.AdvData.Promotions[number.CurrentNumber].SaleImageName = picName; 
				advertisementEditorPanelConfiguration.UpdatePanelConfiguration(advertisementEditorPanelConfiguration.AdvData, advertisementEditorPanelConfiguration.advertisementType, advertisementEditorPanelConfiguration.AdvIndex);
				break;
			}
			case ImageName.QuizesBackground:
			{
				advertisementEditorPanelConfiguration.AdvData.Quizes[number.CurrentNumber].BackgroundImageName = picName; 
				advertisementEditorPanelConfiguration.UpdatePanelConfiguration(advertisementEditorPanelConfiguration.AdvData, advertisementEditorPanelConfiguration.advertisementType, advertisementEditorPanelConfiguration.AdvIndex);
				break;
			}
			case ImageName.QuizesLogo:
			{
				advertisementEditorPanelConfiguration.AdvData.Quizes[number.CurrentNumber].LogoImageName = picName; 
				advertisementEditorPanelConfiguration.UpdatePanelConfiguration(advertisementEditorPanelConfiguration.AdvData, advertisementEditorPanelConfiguration.advertisementType, advertisementEditorPanelConfiguration.AdvIndex);
				break;
			}
			case ImageName.ScratchLogo:
			{
				advertisementEditorPanelConfiguration.AdvData.ScratchData.LogoImageName = picName; 
				advertisementEditorPanelConfiguration.UpdatePanelConfiguration(advertisementEditorPanelConfiguration.AdvData, advertisementEditorPanelConfiguration.advertisementType, advertisementEditorPanelConfiguration.AdvIndex);
				break;
			}
			case ImageName.ScratchBackground:
			{
				advertisementEditorPanelConfiguration.AdvData.ScratchData.BackgroundImageName = picName; 
				advertisementEditorPanelConfiguration.UpdatePanelConfiguration(advertisementEditorPanelConfiguration.AdvData, advertisementEditorPanelConfiguration.advertisementType, advertisementEditorPanelConfiguration.AdvIndex);
				break;
			}
			}
		}
	}
}
