//
// Number.cs
//
// Created by Berezin Boris on 13.07.2015.
// Copyright (c) 2015 Berezin Boris. All rights reserved.
//
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Geec.Advertisement;
using Geec.Advertisement.EditorApplication;

namespace Geec.Advertisement.EditorApplication
{
	public class Number : MonoBehaviour 
	{
		[SerializeField] private AdvertisementEditorPanelConfiguration advertisementEditorPanelConfiguration = null;
		[SerializeField] private AdvertisementType advertisementType = AdvertisementType.Promotion;
		[SerializeField] private Text text = null;
		public int CurrentNumber = 0;
		[SerializeField] private int numberAll = 0;
		[SerializeField] private GameObject buttonNext = null;
		[SerializeField] private GameObject buttonPrev = null;
		[SerializeField] private GameObject buttonDel = null;
		[SerializeField] private GameObject panelLeftRight;
		
		
		public int NumberAll
		{
			get {return numberAll;}
			set 
			{			
				numberAll = value;
				if( numberAll > 0 )
				{
					text.text = text.text = (CurrentNumber + 1).ToString("f0");
				}
				else
				{
					numberAll = 0;
				}
				
				UpdateControlsVisibility();
			}
		}
		
		void UpdateControlsVisibility()
		{
			if( numberAll <= 0 )
			{
				text.gameObject.SetActive( false );
				buttonNext.SetActive( false );
				buttonPrev.SetActive( false );
				buttonDel.SetActive( false );
				panelLeftRight.SetActive( false );
			}
			else
			{
				text.gameObject.SetActive( true );
				buttonDel.SetActive( true );
				panelLeftRight.SetActive( true );
				
				if( CurrentNumber == 0 )
				{
					buttonPrev.SetActive( false );
				}
				else
				{
					buttonPrev.SetActive( true );
				}
				
				if( CurrentNumber == (numberAll-1) )
				{
					buttonNext.SetActive( false );
				}
				else
				{
					buttonNext.SetActive( true );
				}
			}
		}

		private void OnEnable()
		{
			advertisementEditorPanelConfiguration.AdvIndex = CurrentNumber;
		}

		public void Next() 
		{
			CurrentNumber++;
			if( CurrentNumber >= numberAll )
			{
				CurrentNumber = numberAll-1;
			}
			text.text = (CurrentNumber + 1).ToString("f0");
			UpdateControlsVisibility();
			
			advertisementEditorPanelConfiguration.AdvIndex = CurrentNumber;
			advertisementEditorPanelConfiguration.UpdatePanelConfiguration(advertisementEditorPanelConfiguration.AdvData, advertisementType, CurrentNumber);
		}

		public void Previous() 
		{
			CurrentNumber--;
			if( CurrentNumber < 0 )
			{
				CurrentNumber = 0;
			}
			text.text = (CurrentNumber + 1).ToString("f0");
			UpdateControlsVisibility();
			
			advertisementEditorPanelConfiguration.AdvIndex = CurrentNumber;
			advertisementEditorPanelConfiguration.UpdatePanelConfiguration(advertisementEditorPanelConfiguration.AdvData, advertisementType, CurrentNumber);
		}

		public void Add() 
		{
			CurrentNumber = NumberAll;
			NumberAll++;
			text.text = (CurrentNumber + 1).ToString("f0");
			
			if (advertisementType == AdvertisementType.Promotion)
			{
				PromotionData pd = new PromotionData();
				pd.Description = "";
				pd.HttpLink = "";
				advertisementEditorPanelConfiguration.AdvData.Promotions.Add(pd);
			}
			if (advertisementType == AdvertisementType.Quiz)
			{
				QuizData qdata = new QuizData();
				qdata.Question = new QuizData.QuestionData();
				qdata.Question.QuestionText = "";
				qdata.Question.Answer1 = "";
				qdata.Question.Answer2 = "";
				qdata.Question.Answer3 = "";
				qdata.Question.QuestionTextColor = Color.black;			
				qdata.Question.Answer1Color = Color.black;				
				qdata.Question.Answer2Color = Color.black;
				qdata.Question.Answer3Color = Color.black;
				advertisementEditorPanelConfiguration.AdvData.Quizes.Add(qdata);
			}
			advertisementEditorPanelConfiguration.AdvIndex = CurrentNumber;
			advertisementEditorPanelConfiguration.UpdatePanelConfiguration(advertisementEditorPanelConfiguration.AdvData, advertisementType, CurrentNumber);
		}

		public void Remove() 
		{
			if (NumberAll > 0)
			{
				if (advertisementType == AdvertisementType.Promotion)
				{
					advertisementEditorPanelConfiguration.AdvData.Promotions.RemoveAt(CurrentNumber);
				}
				if (advertisementType == AdvertisementType.Quiz)
				{
					advertisementEditorPanelConfiguration.AdvData.Quizes.RemoveAt(CurrentNumber);
				}
				NumberAll--;
				CurrentNumber = Mathf.Min (CurrentNumber, NumberAll-1);
				if (CurrentNumber > -1)
				{
					text.text = (CurrentNumber + 1).ToString("f0");
					advertisementEditorPanelConfiguration.AdvIndex = CurrentNumber;
					advertisementEditorPanelConfiguration.UpdatePanelConfiguration(advertisementEditorPanelConfiguration.AdvData, advertisementType, CurrentNumber);
				}
				
				UpdateControlsVisibility();
			}
		}
	}
}
