﻿//
// FileUtils.cs
//
// Created by Lazarevich Aleksey on 18.05.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//
using UnityEngine;
using System;
using System.Collections;
using System.Xml.Serialization;
using System.IO;


namespace Geec.Utils
{
public class FileUtils : MonoBehaviour 
{
	static string xmlFileExtention = ".xml";

	public static void SerializeToXml<T>( T data, string filePathName )
	{
		try
		{		
			var serializer = new XmlSerializer( typeof(T) );
			
			if( !filePathName.Contains( xmlFileExtention ) )
			{
				filePathName = filePathName + xmlFileExtention;
			}
			
			using( StreamWriter sw = new StreamWriter( filePathName, false, System.Text.Encoding.UTF8 ) )
			{
				serializer.Serialize( sw, data );
			}
			
			Debug.LogWarning( filePathName + " serialization SUCCESS" );
		}
		catch( Exception ex )
		{
			Debug.LogError( "FileUtils::SerializeToXml(): " + ex.Message );
		}
	}
	
	public static T DeserializeFromResourcesXml<T>( string filePathName )
	{
		T data = default(T);
		
		try
		{		
			TextAsset xml = Resources.Load<TextAsset>( filePathName );
			
			if( xml != null )
			{
				var serializer = new XmlSerializer( typeof(T) );
				using( MemoryStream stream = new MemoryStream( xml.bytes ) )
				{
					data = (T)serializer.Deserialize(stream);
				}
			}
			else
			{
				Debug.LogWarning( "FileUtils::DeserializeFromResourcesXml(): Can't load " + filePathName );
			}
		}
		catch( Exception ex ) 
		{
			Debug.LogError( "FileUtils::DeserializeFromResourcesXml(): " + ex.Message );
		}
		
		return data;
	}
	
	
	public static T DeserializeFromXml<T>( string filePathName )
	{
		T data = default(T);
		
		try
		{
			var serializer = new XmlSerializer( typeof(T) );
			
			using( StreamReader sw = new StreamReader( filePathName, System.Text.Encoding.UTF8 ) )
			{
				data = (T)serializer.Deserialize( sw );
			}
		}
		catch( Exception ex ) 
		{
			Debug.LogError( "FileUtils::DeserializeFromXml(): " + ex.Message );
		}
		
		return data;
	}
}
}