//
// AdvertismentConfiguratorInspector.cs
//
// Created by Lazarevich Aleksey on 28.04.2015.
// Copyright (c) 2015 Lazarevich Aleksey. All rights reserved.
//

using UnityEngine;
using UnityEditor;
using System.Collections;

namespace Geec.Advertisement
{
[CustomEditor (typeof(AdvertisementConfigurator))]
public class AdvertismentConfiguratorInspector : Editor 
{
	AdvertisementConfigurator configurator;
	
	void OnEnable()
	{
		configurator = (AdvertisementConfigurator)target;
	}
	
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();
	
		DrawSeparator();
		
		if( GUILayout.Button( "Load" ) )
		{
			configurator.OnInspectorLoad();
		}
		
		if( GUILayout.Button( "Save" ) )
		{
			configurator.OnInspectorSave();
		}
	}
	
	void DrawSeparator()
	{
		EditorGUILayout.Space();

		GUILayout.Box( "", GUILayout.ExpandWidth(true), GUILayout.Height(2) );

		EditorGUILayout.Space();
	}
}
}